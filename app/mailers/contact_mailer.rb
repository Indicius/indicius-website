class ContactMailer < ApplicationMailer
  default from: 'site@indicius.com'

  def form_notification(fields)
    @fields = fields
    mail to: 'contact@indicius.com', subject: 'Contact from site', reply_to: @fields.address
  end
end
