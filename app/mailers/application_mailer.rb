class ApplicationMailer < ActionMailer::Base
  default from: 'site@indicius.com'
  layout nil
end
