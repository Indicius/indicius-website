class CaseStudyPresenter < BasePresenter

  def type_class
    CaseStudy::STYLES.key(@model.block_style)
  end

  def block_id
    "case-study-block-#{@model.block_template}"
  end

  def block_template
    "case_studies/blocks/#{@model.block_template}"
  end

end
