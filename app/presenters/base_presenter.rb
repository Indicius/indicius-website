require 'active_support/core_ext/string/inflections'

class BasePresenter < SimpleDelegator

  def initialize(model, view)
    @model, @view = model, view
    super(@model)
  end

  def render
    @view.render partial: "presenters/#{@model.class.name.underscore}", locals: { presenter: self }
  end

  def to_s
    self.class.name
  end

end
