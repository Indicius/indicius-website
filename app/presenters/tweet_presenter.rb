class TweetPresenter < BasePresenter

  def url
    "https://twitter.com/Indicius/status/#{@model.tweet_id}"
  end

end
