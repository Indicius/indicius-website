class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :http_authenticate

  def not_found
    fail ActionController::RoutingError.new('Not Found')
  end

  protected

  def http_authenticate
    return unless Rails.env.staging?
    authenticate_or_request_with_http_basic do |username, password|
      username == Rails.application.credentials.staging[:user] && password == Rails.application.credentials.staging[:password]
    end
  end
end
