module Admin
  class TweetsController < AdminController

    before_action :set_params, only: [:add, :remove, :update_position]

    def index
      @results = ::TwitterApi.tweets
    end

    def add
      grid_item = GridItem.new(position: @position, gridable_type: 'Tweet')
      if !Tweet.limit_reached? && grid_item.save
        Tweet.update_positions
        msg = { success: 'Tweet position added.' }
      else
        msg = { error: 'Tweet position could not be added.' }
      end
      redirect_to admin_grid_url, msg
    end

    def remove
      grid_item = GridItem.find_by(position: @position)
      if grid_item && grid_item.destroy
        Tweet.update_positions
        msg = { success: 'Tweet position removed.' }
      else
        msg = { error: 'Tweet position could not be removed.' }
      end
      redirect_to admin_grid_url, msg
    end

    def update_position
      grid_item = GridItem.find_by(position: @position)
      new_grid_item = GridItem.new(position: @new_position, gridable_type: 'Tweet')
      if grid_item.destroy && new_grid_item.save
        Tweet.update_positions
        @notification = { type: :success, msg: 'Tweet position updated.' }
      else
        @notification = { type: :error, msg: 'Tweet position could not be updated.' }
      end
      respond_to do |format|
        format.js { render partial: 'notification' }
      end
    end

    private

    def set_params
      @new_position = params[:new_position] if params[:new_position]
      @position = params[:position]
    end

  end
end
