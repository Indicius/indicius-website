module Admin
  class ImagesController < AdminController

    before_action :set_record, only: [:edit, :update, :destroy, :update_position]
    before_action :set_edit_data, only: [:edit, :update]
    before_action :set_create_data, only: [:new, :create]

    def new
      @image = Image.new
    end

    def edit
    end

    def create
      @image = Image.new(safe_params)

      if @image.save
        @image.create_grid_item position: @position
        redirect_to admin_grid_url, success: 'Image was successfully created.'
      else
        render :new
      end
    end

    def update
      if safe_params && @image.update(safe_params)
        redirect_to admin_grid_url, success: 'Image was successfully updated.'
      else
        render :edit
      end
    end

    def destroy
      @image.destroy
      redirect_to admin_grid_url, success: 'Image was successfully deleted.'
    end

    def update_position
      if @image.grid_item.update(position: params[:position])
        @notification = { type: :success, msg: 'Image position updated.' }
      else
        @notification = { type: :error, msg: 'Image position could not be updated.' }
      end
      respond_to do |format|
        format.js { render partial: 'notification' }
      end
    end

    private

    def set_record
      @image = Image.find(params[:id])
    end

    def set_edit_data
      @form_url = admin_image_path(params[:id])
    end

    def set_create_data
      @position = params[:position]
      @form_url = admin_images_path(@position)
    end

    def safe_params
      return nil unless params[:image]
      params.require(:image).permit(:link, :grid_image)
    end

  end
end
