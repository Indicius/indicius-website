module Admin
  class CaseStudiesController < AdminController

    before_action :set_record, only: [:edit, :update, :destroy, :update_position]
    before_action :set_collections
    before_action :set_edit_data, only: [:edit, :update]
    before_action :set_create_data, only: [:new, :create]

    def new
      @case_study = CaseStudy.new(block_style: @style)
    end

    def edit
    end

    def create
      @case_study = CaseStudy.new(safe_params)

      if @case_study.save
        @case_study.create_grid_item position: @position
        redirect_to admin_grid_url, success: 'Case Study was successfully created.'
      else
        render :new
      end
    end

    def update
      if safe_params && @case_study.update(safe_params)
        redirect_to admin_grid_url, success: 'Case Study was successfully updated.'
      else
        render :edit
      end
    end

    def destroy
      @case_study.destroy
      redirect_to admin_grid_url, success: 'Case Study was successfully deleted.'
    end

    def update_position
      if @case_study.grid_item.update(position: params[:position])
        @notification = { type: :success, msg: 'Case study position updated.' }
      else
        @notification = { type: :error, msg: 'Case study position could not be updated.' }
      end
      respond_to do |format|
        format.js { render partial: 'notification' }
      end
    end

    private

    def set_record
      @case_study = CaseStudy.find(params[:id])
    end

    def set_collections
      @page_templates = templates_in_folder('pages')
      @block_templates = templates_in_folder('blocks')
      @link_templates = templates_in_folder('links')
      @case_studies = CaseStudy.linkeable.excluding(@case_study)
    end

    def set_edit_data
      @form_url = admin_case_study_path(params[:id])
    end

    def set_create_data
      @style = params[:style]
      @position = params[:position]
      @form_url = admin_case_studies_path(@style, @position)
    end

    def safe_params
      return nil unless params[:case_study]
      params.require(:case_study).permit(
        :title,
        :slug,
        :block_style,
        :page_template,
        :block_template,
        :link_template,
        :left_link_id,
        :right_link_id
      )
    end

    def templates_in_folder(folder)
      templates = Dir.entries(Rails.root.join('app', 'views', 'case_studies', folder))
      templates.map { |f| f.split('.').first.gsub(/^_/, '') unless File.directory? f }.compact
    rescue
      []
    end

  end
end
