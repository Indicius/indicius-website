module Admin
  class DashboardController < AdminController

    def index
      @missing_shots = Shot.where('processed=? OR visible=?', false, false).to_a
      @totals = {
        total_tweets: Tweet.unscoped.count(1),
        total_shots: Shot.unscoped.count(1),
        total_shots_missing: @missing_shots.size,
        total_videos: Video.unscoped.count(1)
      }
      @grid_diversity = GridItem.unscoped
        .group(:gridable_type)
        .order('count_all DESC').count

      @issues = []
      check_for_missing_shots
    end

    private

    def check_for_missing_shots
      return unless @missing_shots.any?
      @missing_shots.each do |shot|
        if !shot.processed
          @issues << "Shot <a href=\"#{shot.url}\" target=\"_blank\">#{shot.shot_id}</a> was imported but couldn't be processed.".html_safe
        elsif !shot.visible
          @issues << "Shot <a href=\"#{shot.url}\" target=\"_blank\">#{shot.shot_id}</a> is not currently visible.".html_safe
        end
      end
    end

  end
end
