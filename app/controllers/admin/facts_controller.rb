module Admin
  class FactsController < AdminController

    before_action :set_variables

    def add
      if Fact.of_type @type
        add_fact_at_position
        @notification = { type: :success, msg: 'Fact position was added.' }
      else
        @notification = { type: :error, msg: 'Fact position could not be added.' }
      end

      respond_to do |format|
        format.js { render partial: 'notification' }
        format.html { redirect_to admin_grid_url, msg_to_flash(@notification) }
      end
    end

    def remove
      grid_item = GridItem.find_by(position: @position)
      if grid_item.destroy
        msg = { success: 'Fact position was successfully removed.' }
      else
        msg = { success: 'Fact position could not be removed.' }
      end
      redirect_to admin_grid_url, msg
    end

    private

    def add_fact_at_position
      fact = "#{@type}_fact".classify.constantize.first
      fact.grid_item.destroy if fact.grid_item
      fact.create_grid_item position: @position
    end

    def msg_to_flash(notification)
      { "#{notification[:type]}".to_sym => notification[:msg] }
    end

    def set_variables
      @type = params[:type].downcase if params[:type]
      @position = params[:position]
    end

  end
end
