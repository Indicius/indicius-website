module Admin
  class ShotsController < AdminController
    def index
      @shots = Shot.order('timestamp DESC')
    end

    def change_status
      @shot = Shot.find(params[:id])
      @shot.toggle(:visible).save!
      Rails.cache.delete_matched("shot__*")
    end
  end
end
