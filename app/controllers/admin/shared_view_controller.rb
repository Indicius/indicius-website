module Admin
  class SharedViewController < AdminController

    def index
      @pages = []
      grid_items = GridItem.includes(:gridable).to_a
      @total_pages = calculate_total_pages(grid_items)
      @total_pages.times do |page_number|
        @pages << grid_page(grid_items, page_number)
      end
    end

    def add_page
      @page_number = params[:page_number].to_i
      respond_to do |format|
        format.js
      end
    end

    protected

    def calculate_total_pages(grid_items)
      return 1 unless grid_items.any?
      [((grid_items.last.position + 1) / GRID[:page_size].to_f).ceil, 2].max
    end

    def grid_page(items, page_number)
      page = Array.new(GRID[:page_size])
      return page unless items.any?
      GRID[:page_size].times do |position|
        position_in_grid = position + page_number * GRID[:page_size]
        if !page[position] && items.first && items.first.position == position_in_grid
          page = add_item_to_page(position, items.shift.gridable, page)
        end
      end
      page
    end

    def add_item_to_page(position, item, page)
      page[position] = item
      if item.class.name.eql? 'CaseStudy'
        if item.block_style == CaseStudy::STYLES[:vertical]
          page[position + GRID[:row_size]] = item
        else
          page[position + 1] = item
        end
      end
      page
    end

  end
end
