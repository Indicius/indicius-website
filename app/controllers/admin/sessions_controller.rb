module Admin
  class SessionsController < AdminController

    skip_before_action :user_must_log_in
    before_action :user_already_logged_in, except: [:destroy]

    layout 'login'

    def new
    end

    def create
      if log_in(params[:session][:username], params[:session][:password])
        redirect_to(admin_dashboard_path) && return
      end
      render :new
    end

    def destroy
      log_out
      redirect_to admin_login_path
    end

    private

    def user_already_logged_in
      redirect_to(admin_dashboard_path) if logged_in?
    end

  end
end
