class AdminController < ApplicationController

  skip_before_action :http_authenticate
  before_action :user_must_log_in
  layout 'admin'

  add_flash_types :error, :success

  def logged_in?
    session[:logged_in]
  end

  def log_in(username, password)
    return false if username != Rails.application.credentials.admin[:user] || password != Rails.application.credentials.admin[:password]
    session[:logged_in] = true
  end

  def log_out
    session.delete(:logged_in)
  end

  protected

  def user_must_log_in
    redirect_to(admin_login_path) unless logged_in?
  end

end
