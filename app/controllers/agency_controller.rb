class AgencyController < ApplicationController

  skip_before_action :http_authenticate

  def listing
    render layout: 'agency', status: params[:slug]
  end
end
