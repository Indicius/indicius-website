class ContactController < ApplicationController

  skip_before_action :verify_authenticity_token

  def validate_and_send
    result = {
      success: false
    }

    form = ContactForm.new(safe_params)

    if form.valid?
      ContactMailer.form_notification(form).deliver
      result[:success] = true
    else
      result[:errors] = form.errors.keys
    end

    render json: result
  end

  private

  def safe_params
    params.require(:contact).permit(:name, :address, :message, :verification)
  end

end
