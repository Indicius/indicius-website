class LetsTalkController < ApplicationController

  skip_before_action :http_authenticate

  def listing
    render layout: 'lets-talk', status: params[:slug]
  end
end
