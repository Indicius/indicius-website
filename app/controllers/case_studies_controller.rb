class CaseStudiesController < ApplicationController

  skip_before_action :verify_authenticity_token

  def index
    @case_study = CaseStudy.includes(:left_link, :right_link).find_by(slug: params[:slug])
    template = template_path
    if @case_study && template
      render template: template, layout: 'case-studies'
    else
      not_found
    end
  end

  def listing
    render layout: 'cases', status: params[:slug]
  end

  private

  def template_path
    pages_path = ['case_studies/pages']
    unless @case_study && lookup_context.exists?(@case_study.page_template, pages_path)
      return nil
    end
    File.join(pages_path, @case_study.page_template)
  end

end
