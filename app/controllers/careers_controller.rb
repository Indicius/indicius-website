class CareersController < ApplicationController

  skip_before_action :http_authenticate

  def listing
    render status: :listing
  end
end
