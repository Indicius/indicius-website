class WhatWeDoController < ApplicationController

  skip_before_action :http_authenticate

  def listing
    render layout: 'what_we_do', status: params[:slug]
  end
end
