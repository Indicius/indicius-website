class GridController < ApplicationController

  skip_before_action :verify_authenticity_token

  def index
    @debug = params[:debug].nil? ? false : true;
    @starting_position = params[:start].nil? ? 0 : params[:start].to_i.not_negative
    @items = []

    reset_fill_pointers
    prepare_items

    if params[:start].nil?
      respond_to :html
    else
      respond_to :js
    end
  end

  private

  def count_grid_items(grid_items)
    grid_items.size + grid_items.count { |i| i.gridable_type.eql? 'CaseStudy' }
  end

  def reset_fill_pointers
    session[:last_video] = session[:last_shot] = Time.now.utc.to_i if @starting_position == 0
  end

  # def prepare_items
  #   grid_items = fetch_grid_items
  #   fill_items = fetch_fill_items(grid_items.size)
  #   @total_items = grid_items.size + fill_items.count
  #   populate_items(grid_items, fill_items)
  # end

   def prepare_items
    grid_items = fetch_grid_items
    grid_items_count = count_grid_items(grid_items)
    fill_items = fetch_fill_items(grid_items_count)
    @total_items = grid_items_count + fill_items.count
    populate_items(grid_items, fill_items)
   end

  def populate_items(grid_items, fill_items)
    taken_positions = []
    GRID[:page_size].times do |t|
      position = t + @starting_position
      if grid_items.any? && grid_items.first.position == position
        @items << grid_items.shift.gridable
        if @items.last.class.name.eql? 'CaseStudy'
          taken_positions << position + (@items.last.block_style == 2 ? 1 : GRID[:row_size])
        end
      elsif fill_items.any? && !taken_positions.include?(position)
        @items << fill_items.shift
      end
    end
  end

  def fetch_grid_items
    GridItem.cached_items(@starting_position)
  end

  def fetch_fill_items(total_grid_items)
    return [] if total_grid_items == GRID[:page_size]

    shots_amount, videos_amount = fill_items_amounts(total_grid_items)
    videos = fetch_videos(videos_amount)

    return videos if shots_amount == 0
    mix_items_into(fetch_shots(shots_amount), videos)
  end

  def fill_items_amounts(items_count)
    shots_left = count_shots_left

    videos_default_amount = items_count > 0 ? 2 : 3
    videos_amount = if shots_left < GRID[:page_size]
      [GRID[:page_size] - items_count - shots_left, videos_default_amount].max
    else
      videos_default_amount
    end

    shots_amount = GRID[:page_size] - items_count - videos_amount
    [shots_amount, videos_amount]
  end

  def mix_items_into(target, items)
    start_position = 0
    items.each do |item|
      end_position = target.size
      insert_position = start_position == end_position ? end_position : rand(start_position...end_position)
      target.insert(insert_position, item)
      start_position = insert_position + 1
    end
    target
  end

  def fetch_videos(amount)
    videos = Video.cached_videos(@starting_position, session[:last_video], amount)
    session[:last_video] = videos.last.timestamp if videos.any?
    videos
  end

  def fetch_shots(amount)
    shots = Shot.cached_shots(@starting_position, session[:last_shot], amount)
    session[:last_shot] = shots.last.timestamp if shots.any?
    shots
  end

  def count_shots_left
    Shot.cached_shots_left(@starting_position, session[:last_shot])
  end

end
