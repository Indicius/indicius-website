class WhoWeAreController < ApplicationController

  skip_before_action :http_authenticate

  def listing
    render layout: 'who_we_are', status: params[:slug]
  end
end
