class ErrorsController < ApplicationController

  skip_before_action :http_authenticate

  def not_found
    render status: :not_found
  end
end
