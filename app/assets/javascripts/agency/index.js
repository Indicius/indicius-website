var viewportWidth = window.innerWidth

// -------------- SPLIT PAGE LOTTIES --------------
var splitPageExists = document.getElementById('split-page')
if (splitPageExists) {
  // "What we do" background
  var whatWeDoBackground = lottie.loadAnimation({
    container: document.getElementById('what-we-do-background'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'https://agency-section.indicius.now.sh/assets/json/waves-background.json'
  })

  // "Who we are" background
  var whoWeAre = lottie.loadAnimation({
    container: document.getElementById('who-we-are-background'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'https://agency-section.indicius.now.sh/assets/json/waves-background.json'
  })

  // "What we do" illustration
  var whatWeDoIllustration = lottie.loadAnimation({
    container: document.getElementById('what-we-do-illustration'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'https://agency-section.indicius.now.sh/assets/json/what-we-do-illustration.json'
  })

  // "Who we are" illustration
  var whoWeAreIllustration = lottie.loadAnimation({
    container: document.getElementById('who-we-are-illustration'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'https://agency-section.indicius.now.sh/assets/json/who-we-are-illustration.json'
  })
}

// -------------- HEROS LOTTIES --------------
var heroExists = document.getElementById('new_agency_hero')
if (heroExists) {
  // Hero's Background Lottie
  var heroBackground = lottie.loadAnimation({
    container: document.getElementById('hero__background'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: viewportWidth >= 1024 ? 'https://agency-section.indicius.now.sh/assets/json/hero-background-animation.json' : 'https://agency-section.indicius.now.sh/assets/json/hero-background-animation-mobile.json'
  })
}

// -------------- "WHO WE ARE" LOTTIES --------------
var whoWeAreExists = document.getElementById('who-we-are')
if (whoWeAreExists) {
  // "Culture" car Lottie
  var cultureAnimation = lottie.loadAnimation({
    container: document.getElementById('culture__illustration'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'https://agency-section.indicius.now.sh/assets/json/car.json',
    rendererSettings: {
      preserveAspectRatio: 'xMidYMin meet'
    }
  })
}

// -------------- MASONRY GRID --------------
var masonryExists = document.getElementById('whoWeAreGallery')

function initMasonry() {
  if (masonryExists && viewportWidth >= 1024) {
    var masonry = new Masonry('.gallery__grid', {
      itemSelector: '.grid__item',
      gutter: 32,
      percentPosition: true
      // horizontalOrder: true
    });
  }
}

window.addEventListener('resize', initMasonry)
imagesLoaded('.gallery__grid', initMasonry)