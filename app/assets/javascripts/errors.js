//= require jquery

function centerWrapper() {
  var negativeTopMargin  = - $('.wrapper').outerHeight() / 2;
  var negativeLeftMargin = - $('.wrapper').outerWidth() / 2;

  $('.wrapper').css({
    'margin-top' : negativeTopMargin + 'px',
    'margin-left': negativeLeftMargin + 'px'
  });
}

$(document).ready(function() {
  centerWrapper();
});

$(window).on('load resize', function() {
  centerWrapper();
});
