;(function(window,$,undefined) {

  $(document).ready(function(){
    $('.phone-indicators .phone-indicator').on('click', function() {
      $('.phone-indicators .phone-indicator').removeClass('is-active');

      $(this).addClass('is-active');
  
      var destination = $(this).attr('data-slide');

      $('.phone-frame-carousel').animate({scrollTop: $('.phone-frame-carousel').height() * destination}, 1500, 'easeInOutQuart');
    });
  });

})(window,jQuery);
