;(function(window,$,undefined) {

    $(document).ready(function(){
      $finalVersionTabs = $('.js-samsung-final-version-tabs button'),
      $finalVersionPanels = $('.js-samsung-final-version-panels');

      $finalVersionTabs.on('click', function() {
        $finalVersionTabs.removeClass('active');
        $(this).addClass('active');

        var target = $(this).attr('data-target');

        $finalVersionPanels.find('.panel').hide();
        $finalVersionPanels.find('[data-panel="' + target + '"]').show();
      });

      $('.js-vertical-cards-carousel').slick({
        dots: false,
        controls: false,
        arrows: false,
        infinite: true,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });        
    });
  
  })(window,jQuery);
  