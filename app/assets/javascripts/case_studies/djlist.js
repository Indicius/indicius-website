;(function(window,$,undefined) {

  function ready() {
    //console.log('djlist.js');
    $(window).on('load resize', function() {
      $('img.waves').css({
        'bottom': - $('img.waves').outerHeight() / 2,
        'margin-left': - $('img.waves').outerWidth() / 2
      });

      $('img.showcase').css({
        'bottom': - $('img.showcase').outerHeight() / 2,
        'margin-left': - $('img.showcase').outerWidth() / 2
      });
    });
  }

  $(document).ready(function(){
    if($('img.waves').size()) {
      ready();
    }
  });

})(window,jQuery);
