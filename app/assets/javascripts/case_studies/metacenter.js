;(function(window,$,undefined) {

    $(document).ready(function(){
      var $handleControlInput = $('#metacenter-handle-control-input'),
          $filledValueBar = $('.js-metacenter-filled-value'),
          $afterMask = $('.js-metacenter-after-mask');

      $handleControlInput.on('change mousemove', function() {
        var value = parseInt($(this).val());

        $filledValueBar.css('width', 100 - value + '%');
        $afterMask.css('width', 100 - value + '%');
      });

      $('.js-metacenter-moodboard-slider').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        dots: false,
      });

      $('.js-metacenter-teams-cards-slider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        dots: true,
      });

      $('.js-icon-cards').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: false,
        arrows: true,
        dots: false,
      });

      var $handleControlInput2 = $('#metacenter-handle-control-input-2'),
          $filledValueBar2 = $('.js-metacenter-filled-value-2'),
          $afterMask2 = $('.js-metacenter-after-mask-2');

      $handleControlInput2.on('change mousemove', function() {
        var value = parseInt($(this).val());

        $filledValueBar2.css('width', 100 - value + '%');
        $afterMask2.css('width', 100 - value + '%');
      });
      
      var $animatedSvgContainer = $('#metacenter-graphic-animation-wrapper');

      if ($animatedSvgContainer.length > 0) {
        bodymovin.loadAnimation({
          container: document.getElementById('metacenter-graphic-animation-wrapper'),
          renderer: 'svg',
          loop: true,
          autoplay: true,
          animationData: JSON.parse($('#metacenter-graphic-animation').text())
        });
      }
      
      var $animatedSvgContainer2 = $('#metacenter-dots-map-animation-wrapper');

      if ($animatedSvgContainer2.length > 0) {
        bodymovin.loadAnimation({
          container: document.getElementById('metacenter-dots-map-animation-wrapper'),
          renderer: 'svg',
          loop: true,
          autoplay: true,
          animationData: JSON.parse($('#metacenter-dots-map-animation').text())
        });
      }
    });

    if ($(window).width() >= 1140) {
      $('#designConsistentlyPin').pin({
        containerSelector: '#designConsistentlyDuration',
        padding: {
          top: ($(window).height() / 2) - ($('#designConsistentlyPin').outerHeight() / 2),
          bottom: 0
        }
      });

      $('#scalePin').pin({
        containerSelector: '#scaleDuration',
        padding: {
          top: ($(window).height() / 2) - ($('#scalePin').outerHeight() / 2),
          bottom: 0
        }
      });
    }
  
  })(window,jQuery);
  