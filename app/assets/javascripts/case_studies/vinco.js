;(function(window,$,undefined) {
    $('.branding-slick').slick({
        dots: false,
        controls: false,
        arrows: false,
        infinite: true,
        speed: 250,
        autoplay: true,
        autoplaySpeed: 2500,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.card-slide-container').slick({
        dots: false,
        controls: false,
        arrows: false,
        infinite: true,
        speed: 220,
        autoplay: true,
        autoplaySpeed: 1700,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
    });

    var $animatedSvgContainer = $('#animated-svg-container-vinco');

    if ($animatedSvgContainer.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('animated-svg-container-vinco'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#animated-svg-container-vinco code').text())
        });
    }

    var $thankAnimation = $('#thanks-animation');

    if ($thankAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('thanks-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#thanks-animation code').text())
        });
    }

    var $firstUiAnimation = $('#first-ui-animation');

    if ($firstUiAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('first-ui-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#first-ui-animation code').text())
        });
    }

    var $secondUiMotionAnimation = $('#second-ui-motion-animation');

    if ($secondUiMotionAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('second-ui-motion-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#second-ui-motion-animation code').text())
        });
    }

    var $thirdUiAnimation = $('#third-ui-animation');

    if ($thirdUiAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('third-ui-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#third-ui-animation code').text())
        });
    }
    
    var $fourthUiAnimation = $('#fourth-ui-animation');

    if ($fourthUiAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('fourth-ui-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#fourth-ui-animation code').text())
        });
    }

    var $fifthUiAnimation = $('#fifth-ui-animation');

    if ($fifthUiAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('fifth-ui-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#fifth-ui-animation code').text())
        });
    }
    
    var $secondWhiteAnimation = $('#second-white-animation');

    if ($secondWhiteAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('second-white-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#second-white-animation code').text())
        });
    }

    var $thirdBlueAnimation = $('#third-blue-animation');

    if ($thirdBlueAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('third-blue-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#third-blue-animation code').text())
        });
    }


    var $fourthBlueAnimation = $('#fourth-blue-animation');

    if ($fourthBlueAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('fourth-blue-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#fourth-blue-animation code').text())
        });
    }

    var $fifthWhiteAnimation = $('#fifth-white-animation');

    if ($fifthWhiteAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('fifth-white-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#fifth-white-animation code').text())
        });
    }

    var $sixthBlueAnimation = $('#sixth-blue-animation');

    if ($sixthBlueAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('sixth-blue-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#sixth-blue-animation code').text())
        });
    }

    var $seventhBlueAnimation = $('#seventh-blue-animation');

    if ($seventhBlueAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('seventh-blue-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#seventh-blue-animation code').text())
        });
    }

    var $brandingLogoAnimation = $('#branding-logo-animation');

    if ($brandingLogoAnimation.length > 0) {
        bodymovin.loadAnimation({
            container: document.getElementById('branding-logo-animation'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            animationData: JSON.parse($('#branding-logo-animation code').text())
        });
    }

    var $animatedSvgContainerLogo = $('#animated-svg-logo-vinco');

    if ($animatedSvgContainerLogo.length > 0) {
        var logoAnimation = bodymovin.loadAnimation({
            container: document.getElementById('animated-svg-logo-vinco'),
            renderer: 'svg',
            loop: true,
            autoplay: false,
            animationData: JSON.parse($('#animated-svg-logo-vinco code').text())
        });

        $(window).scroll(function() {
            // calculate the percentage the user has scrolled down the page
            var scrollPercent = 1000 * $(window).scrollTop() / ($(document).height() - $(window).height());
         
            scrollPercentRounded = Math.round(scrollPercent);
    
            logoAnimation.goToAndStop((scrollPercentRounded / 100) * 1000)
        });
    }

    $('.parallax-icon').paroller();

    $('.large-slick-phone').slick({
        draggable: false,
        dots: false,
        controls: false,
        arrows: false,
        infinite: true,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.small-slick-phone').slick({
        draggable: false,
        dots: false,
        controls: false,
        arrows: false,
        infinite: true,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 10,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

})(window,jQuery);