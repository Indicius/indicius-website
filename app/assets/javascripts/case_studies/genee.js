;(function(window,$,undefined) {

  function ready() {
    //console.log('genee.js');

    $('.before-after').twentytwenty();
    centerWireframesText();

    $(window).on('load resize', function() {
      centerWireframesText();
    });
  }

  function centerWireframesText() {
    var element = $('.wireframes .wireframe-step p span.text');
    element.each(function() {
      var textNegativeTopMargin = - element.outerHeight() / 2;
      var textNegativeLeftMargin = - element.outerWidth() / 2;

      element.css({
        'margin-top': textNegativeTopMargin,
        'margin-left': textNegativeLeftMargin,
      });
    });
  }

  $(document).ready(function(){
    if($('.wireframes-genee').size()) {
      ready();
    }
  });

})(window,jQuery);
