;(function(window,$,undefined) {

  function ready() {
    //console.log('polyvibe.js');
    var gridSVG = $('.gridimg').drawsvg({
        duration: 150,
        stagger: 30,
        easing: 'linear'
      });
    var draw = 0;

    // init controller
    var controller = new ScrollMagic.Controller();

    // build scene
    var scene = new ScrollMagic.Scene({triggerElement: ".gridimg", duration: 200})
                    .addTo(controller)
                    .on("enter leave", function (e) {
                      if (draw == 0) {
                        draw = 1;
                        gridSVG.drawsvg('animate')
                      }
                    });

    if ($('.viewer').length > 0) {
      $('.slider-poly').on('init', function(event, slick){
        const video = slick.$slides[0].querySelector('.viewer');
        video.currentTime = 0;
        video.play();
      });
    }

    $('.slider-poly').slick({
      dots: true,
      arrows: false,
      infinite: true,
      speed: 300,
      autoplay: true,
      autoplaySpeed: 14000
    });

    $('.slider-illus').slick({
      dots: false,
      arrows: false,
      infinite: true,
      speed: 800,
      autoplay: true,
      autoplaySpeed: 1000,
      slidesToShow: 11,
      slidesToScroll: 1
    });

    $('.slider-poly').on('beforeChange', function(event, slick, currentSlide, nextSlide){
      const video = slick.$slides[nextSlide].querySelector('.viewer');
      video.currentTime = 0;
      video.play();
    });
  }

  $(document).ready(function(){
    if($('.gridimgcontainer').size()) {
      ready();
    }
  });

})(window,jQuery);
