;(function(window,$,undefined) {

  function ready() {
    //console.log('li5.js');

    $('.ui-slider').slick({
      dots: false,
      arrows: true,
      infinite: false,
      speed: 500
    });

    $('.marquee').slick({
      dots: false,
      arrows: false,
      infinite: false,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 1500,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: true,
      infinite: true,
    });
  }

  $(document).ready(function(){
    if($('.marquee').size()) {
      ready();
    }
  });

})(window,jQuery);
