function goToSlideUI(slide) {
  $('.uianimation img.imageui').removeClass('imageshow');
  $('.lineui').removeClass('color');

  $('.uianimation img.imageui' + slide).addClass('imageshow');
  $('.lineui' + slide).addClass('color');

  actualui = slide;
}

function goToSlide(slide) {
  $('.uxresearch img.image').removeClass('imageshow');
  $('.line').removeClass('color');


  $('.uxresearch img.image' + slide).addClass('imageshow');
  $('.line' + slide).addClass('color');

  actual = slide;
}

;(function(window,$,undefined) {

  function ready() {
    //console.log('red_bull_records.js')
    var actual = 5;
    var actualui = 3;
    $('.line1').addClass('color');
    $('.lineui1').addClass('color');

    setInterval(function() {

      if(actual < 5){
        actual++;
      } else {
        actual = 1;
      }

      goToSlide(actual);

      if(actualui < 3){
        actualui++;
      } else {
        actualui = 1;
      }

      goToSlideUI(actualui);
    }, 4000);

    var tweenElements = function() {
      var triggerOffset = $('.player-wrapper').height() / 2,
          controller = new ScrollMagic.Controller(),
          controllerWire = new ScrollMagic.Controller(),
          animatableStrokes = $('.map-animate-strokes').drawsvg({
            duration: 1400,
            easeing: 'easeInOut',
            stagger: 350
          }),
          shouldAnimateMap = true,
          animatableWire = $('.wire-animate-strokes').drawsvg({
            duration: 1400,
            easeing: 'easeInOut',
            stagger: 350,
            reverse: true
          }),
          shouldAnimateWire = true;

      var getDuration = function() {
        var playerOffset = $('.player-wrapper').offset().top + $('.player-wrapper').height() / 2,
            playButton = document.getElementById('Rectangle-u-18'),
            playButtonHeight = playButton.getBoundingClientRect().height,
            playButtonOffset = $('#Rectangle-u-18').offset().top + playButtonHeight / 2;

        return playButtonOffset - playerOffset;
      };

      var tween = TweenMax.to('.play-button-wrapper', 1, {
        transformOrigin: 'center center',
        height: 380,
        marginTop: '-175px'
      });

      var scene = new ScrollMagic.Scene({
        triggerElement: '.player-wrapper',
        offset: triggerOffset,
        duration: getDuration
      })
        .setPin('.play-button-wrapper')
        .setTween(tween)
        .addTo(controller);

      scene.on('end', function(event) {
        if (shouldAnimateMap === true) {
          animatableStrokes.drawsvg('animate');

          shouldAnimateMap = false;
        } else {
          return;
        }
      });

      var sceneWire = new ScrollMagic.Scene({
        triggerElement: '.wireframes-comp',
      }).addTo(controllerWire);

      sceneWire.on('start', function(event) {
        if (shouldAnimateWire === true) {
          setTimeout(function() {
            animatableWire.drawsvg('animate');

            shouldAnimateWire = false;
          }, 500);
        } else {
          return;
        }
      });
    };

    if ($(window).width() > 414) {
      tweenElements();
    }
  }

  $(document).ready(function(){
    if($('#case-study-page-red_bull_records').size()) {
      ready();
    }
  });

})(window,jQuery);

