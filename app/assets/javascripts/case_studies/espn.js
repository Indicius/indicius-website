;(function(window,$,undefined) {

  function ready() {
    //console.log('espn.js');
    if ( $(window).width() > 768) {
      alignImages();
    } else if ( $(window).width() <= 768 ) {
      alignImages();
      $('.broadcast img.la-map').attr('style', '');
    }

    $(window).on('load resize', function() {
      if ( $(window).width() > 768) {
        alignImages();
      } else if ( $(window).width() <= 768 ) {
        alignImages();
        $('.broadcast img.la-map').attr('style', '');
      }
    });
  }

  function alignImages() {
    var lowerThirdsWidth = $('.on-screen img.lower-thirds').outerWidth();
    $('.on-screen img.lower-thirds').css('margin-left', -lowerThirdsWidth/2);

    var laMapHeight = $('.broadcast img.la-map').outerHeight();
    $('.broadcast img.la-map').css('margin-top', -laMapHeight/2);
  }

  $(document).ready(function(){
    if($('.broadcast').size()) {
      ready();
    }
  });

})(window,jQuery);
