;(function(window,$,undefined) {

  function ready() {
    //console.log('tn.js');
    var $videographSlideButton = $('.videograph [data-go]'),
        currentSlide = 1,
        rotatedVideograph;

    var changeVideographSlide = function() {
      var slideNumber = $(this).attr('data-go'),
          $slides = $('.videograph .slider-images .slide'),
          $text = $('.videograph .slider-text .slide'),
          currentColor = $('.videograph .slider-images .slide[data-slide="' + slideNumber + '"]').attr('data-color');

      clearInterval(rotatedVideograph);

      $slides.removeClass('active');
      $text.removeClass('active');
      $videographSlideButton.removeClass('active');

      $('#videographBg').css({
        fill: currentColor
      });

      $('.iconography .bottom').css({
        color: currentColor
      })

      $('.videograph').css({
        background: currentColor
      });

      $('.videograph .slider-images .slide[data-slide="' + slideNumber + '"]').addClass('active');
      $('.videograph .slider-text .slide[data-slide="' + slideNumber + '"]').addClass('active');
      $('.videograph [data-go="' + slideNumber + '"]').addClass('active');
    };

    var rotateVideographSlides = function() {
      var $slides = $('.videograph .slider-images .slide'),
          $text = $('.videograph .slider-text .slide');

      rotatedVideograph = setInterval(function() {
        currentSlide += 1;

        if (currentSlide > $slides.length) {
          currentSlide = 1;
        }

        var currentColor = $('.videograph .slider-images .slide[data-slide="' + currentSlide + '"]').attr('data-color');

        $slides.removeClass('active');
        $text.removeClass('active');
        $videographSlideButton.removeClass('active');

        $('#videographBg').css({
          fill: currentColor
        });

        $('.iconography .bottom').css({
          color: currentColor
        })

        $('.videograph').css({
          background: currentColor
        });

        $('.videograph .slider-images .slide[data-slide="' + currentSlide + '"]').addClass('active');
        $('.videograph .slider-text .slide[data-slide="' + currentSlide + '"]').addClass('active');
        $('.videograph [data-go="' + currentSlide + '"]').addClass('active');
      }, 5000);
    };

    $videographSlideButton.on('click', changeVideographSlide);
    rotateVideographSlides();

    $('.onscreen-slider').slick({
      dots: false,
      arrows: false,
      infinite: true,
      speed: 750,
      slidesToShow: 1,
      centerMode: true,
      variableWidth: true,
      autoplay: true,
      focusOnSelect: true
    });
  }

  $(document).ready(function(){
    if($('.videograph').size()) {
      ready();
    }
  });

})(window,jQuery);
