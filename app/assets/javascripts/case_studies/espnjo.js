;(function(window,$,undefined) {

  function ready() {
    //console.log('espnjo.js');
    var hiddentv = true;
    alignImages();

    $(window).on('load resize', function() {
        alignImages();
    });

    $(window).on('scroll', function() {
      if (hiddentv && $('.lower-thirds').length && ($('.lower-thirds').offset().top - $(window).scrollTop()) < 100 && $(window).width() > 900) {
        showTv();
        hiddentv = false;
      }
    });
  }

  function alignImages() {
    var lowerThirdsWidth = $('.on-screen img.lower-thirds').outerWidth();
    $('.on-screen img.lower-thirds').css('margin-left', -lowerThirdsWidth/2);

    var laMapHeight = $('.broadcast img.la-map').outerHeight();
    $('.broadcast img.la-map').css('margin-top', - laMapHeight/2);

    var officialHeight = $('.official-broadcaster .container img').outerHeight() * 1.4;
    $('.official-broadcaster').css('height', officialHeight);

    var kickoffHeight = $('.kickoff img.image2').outerHeight() * 1.8;
    $('.kickoff').css('height', kickoffHeight);

    var transitionsHeight = $('.transitions').outerWidth() / 2.7;
    $('.transitions').css('height', transitionsHeight);
  }

  function showTv() {
    setTimeout(function(){
      $('.circle').fadeIn(500);
    }, 400);
    setTimeout(function(){
      $('.green').animate(
        {
          "left": "0px",
          "opacity": "1"
        }
      , "slow");
    }, 500);
    setTimeout(function(){
      $('.white').animate(
        {
          "left": "37px",
          "opacity": "1"
        }
    , "slow");
    }, 900);
    setTimeout(function(){
      $('.blue').animate(
        {
          "left": "198px",
          "opacity": "1"
        }
      , "slow");
    }, 1000);
    setTimeout(function(){
      $('.text2').animate(
        {
          "top": "179px",
          "opacity": "1"
        }
      , 400);
    }, 1100);
    setTimeout(function(){
      $('.text1').animate(
        {
          "top": "101px",
          "opacity": "1"
        }
      , 400);
    }, 1300);
  }

  $(document).ready(function(){
    if($('.tv').size()) {
      ready();
    }
  });

})(window,jQuery);
