;(function(window,$,undefined) {

  function ready() {
    //console.log('tigo.js');

    $('.slider1').slick({
      centerMode: true,
      centerPadding: '60px',
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      infinite: true,
      speed: 300,
      variableWidth: true,
      prevArrow: '<button type="button" class="slick-prev-custom"><img src="/assets/case-studies/tigo/previous1-fa2c1b221b48ae60f03f827c7517f6d5546dc8db29ad4ce1bf9d70307abd44cb.svg"></button>',
      nextArrow: '<button type="button" class="slick-next-custom"><img src="/assets/case-studies/tigo/next1-00ca00e2b04ba35c45c6810eda0901cdbc35bd8b5773f982453b997f119ef5e4.svg"></button>'
    });

    $('.slider2').slick({
      centerMode: true,
      centerPadding: '60px',
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      infinite: true,
      speed: 300,
      variableWidth: true,
      prevArrow: '<button type="button" class="slick-prev-custom"><img src="/assets/case-studies/tigo/previous2-049cc8552b48cb6b009c51ec7749d558d2c9d2c03f6d57d0b59aeaf66ebea8da.svg"></button>',
      nextArrow: '<button type="button" class="slick-next-custom"><img src="/assets/case-studies/tigo/next2-79038d470171b5d3b950b302537a735bd86fc166b2a53673bd26cc7c6ce1ebd1.svg"></button>'
    });
  }

  $(document).ready(function(){
    if($('.slider1').size()) {
      ready();
    }
  });

})(window,jQuery);
