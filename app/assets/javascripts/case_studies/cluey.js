;(function(window,$,undefined) {

    $(document).ready(function(){
      $('.hero-carousel-indicators .hero-carousel-indicator').on('click', function() {
        $('.hero-carousel-indicators .hero-carousel-indicator').removeClass('is-active');
  
        $(this).addClass('is-active');
    
        var destination = $(this).attr('data-slide');
  
        $('.hero-carousel .content').animate({scrollTop: $('.hero-carousel .content').height() * destination + 1}, 1500, 'easeInOutQuart');
      });

      var $animatedSvgContainer = $('#animated-svg-container');

      if ($animatedSvgContainer.length > 0) {
        bodymovin.loadAnimation({
          container: document.getElementById('animated-svg-container'),
          renderer: 'svg',
          loop: true,
          autoplay: true,
          animationData: JSON.parse($('#animated-svg-container code').text())
        });
      }
    });
  
  })(window,jQuery);
  