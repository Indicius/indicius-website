function socialOverlay() {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    jQuery.noop();
  } else {
    if ($(window).width() >= 1600) {
      var heroHeight = $('#hero').outerHeight() + 600;
      $('#hero .overlay').height(heroHeight);
      $('#heroWrapper .overlay > span').css('top', (heroHeight / 2) - 300);
    } else {
      var heroHeight = $('#hero').outerHeight();
      $('#hero .overlay').height(heroHeight);
    }

    // Removing events from all elements to prevent multiple executions
    $('#hero .social, #hero .social > a').off('mouseenter mouseleave');

    // For Twitter
    $('a#twitter').on('mouseenter', function() {
      $('.menu').addClass('twitter');
      $('.trigger').addClass('social');
    }).on('mouseleave', function() {
      $('.menu').removeClass('twitter');
      $('.trigger').removeClass('social');
    });
    // For Facebook
    $('a#facebook').on('mouseenter', function() {
      $('.menu').addClass('facebook');
      $('.trigger').addClass('social');
    }).on('mouseleave', function() {
      $('.menu').removeClass('facebook');
      $('.trigger').removeClass('social');
    });
    // For Dribbble
    $('a#dribbble').on('mouseenter', function() {
      $('.menu').addClass('dribbble');
      $('.trigger').addClass('social');
    }).on('mouseleave', function() {
      $('.menu').removeClass('dribbble');
      $('.trigger').removeClass('social');
    });
    // For Instagram
    $('a#instagram').on('mouseenter', function() {
      $('.menu').addClass('instagram');
      $('.trigger').addClass('social');
    }).on('mouseleave', function() {
      $('.menu').removeClass('instagram');
      $('.trigger').removeClass('social');
    });
    // For Vimeo
    $('a#vimeo').on('mouseenter', function() {
      $('.menu').addClass('vimeo');
      $('.trigger').addClass('social');
    }).on('mouseleave', function() {
      $('.menu').removeClass('vimeo');
      $('.trigger').removeClass('social');
    });
  }
}
