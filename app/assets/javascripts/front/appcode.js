(function($, window, undefined){

  History.Adapter.bind(window, 'statechange', function(){
    $(document).trigger('history:changed');
  });

  // Scroll to grid when coming to index from a Case Study "View work" link
  $(window).on('load', function() {
    var hash = window.location.hash;
    if (hash === '#theGrid') {
      setTimeout(function() {
        $('html,body').animate({ scrollTop: $('#theGrid').offset().top}, 1000,'easeInOutQuart');
      }, 1200);
    }else{
      setTimeout(function() {
        $('html, body').scrollTop(0);
      }, 0);
    }
  });

  window.prepareNewItems = function() {
    $.loadAsyncImages();
  }

  $(document).ready(function() {

    $(document)

      // Custom resize event
      .on('pageResized', function(e) {
        socialOverlay();
      })


      // Custom ready event
      .on('pageLoaded', function(e) {

        new WOW({
          mobile: false
        }).init();

        // Page resize performance tweaks
        $(document).trigger('pageResized');
        var resizeTimeout = null;

        $(window).off('resize').on('resize', function() {
          clearTimeout(resizeTimeout);
          resizeTimeout = setTimeout(function() {
            $(document).trigger('pageResized');
          }, 200);
        });
      })
      .trigger('pageLoaded')


      // Handle GIFs loading and removal
      .on('mouseover', '[data-animated-version]', function(event) {
        var $target = $(this);
        $.loadImage($target.attr('data-animated-version')).done(function(loaded) {
          $target.append($(loaded).addClass('animated-version'));
        }).fail(function(loaded) {
          // console.error(loaded);
        }).always(function() {
          $target.addClass('loaded');
        });
      })

      .on('mouseout', '[data-animated-version]', function(event) {
        $(this).find('.animated-version').remove();
      });

  });

})(jQuery, window);
