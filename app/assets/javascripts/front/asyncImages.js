$.loadImage = function(url) {
  var loadImage = function(deferred) {
    var image = new Image();
    image.onload = loaded;
    image.onerror = errored; // URL returns 404, etc
    image.onabort = errored; // IE may call this if user clicks "Stop"
    image.src = url;
    function loaded() {
      unbindEvents();
      deferred.resolve(image);
    }
    function errored() {
      unbindEvents();
      deferred.reject(image);
    }
    function unbindEvents() {
      image.onload = null;
      image.onerror = null;
      image.onabort = null;
    }
  };
  return $.Deferred(loadImage).promise();
};

$.loadAsyncImages = function(options) {
  var settings = $.extend({
    onBefore: function(){},
    onDone: function(){},
    onFail: function(image) {
      console.error('Failed to load: ', image);
    }
  }, options || {});

  $('img[data-src]').each(function(index, initial) {
    settings.onBefore.call(this, initial);
    $.loadImage($(initial).attr('data-src')).done(function(loaded) {
      $(initial).replaceWith(loaded);
      settings.onDone.call(this, loaded);
    }).fail(function(loaded) {
      settings.onFail.call(this, loaded);
    });
  });
}
