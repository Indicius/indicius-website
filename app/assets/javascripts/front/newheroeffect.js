;(function(window,$,undefined) {

  function ready() {

    var heroShown = true;
    var doMouseWheel = true;
    var executingScrolling = false;

    $(document).scroll(function(){
        doScroll();
    });

    function backToInit() {
      $("#heroWrapper").removeClass("header-out");
      $(".trigger:not('.scrolltrigger')").removeClass("dissapear");
      $('body').css({'overflow-y': 'hidden'});
      $('body').css({'position': 'fixed'});

      setTimeout(function() {
        doMouseWheel = true;
        $("#heroWrapper").removeClass("header-out-transition");
      }, 1000);

      heroShown = true;
    }

    function hideHero() {
      $("#heroWrapper").addClass("header-out");
      $(".trigger:not('.scrolltrigger')").addClass("dissapear");

      setTimeout(function() {
        $("#heroWrapper").addClass("header-out-transition");
        heroShown = false;
      }, 1000);

    }

    function doScroll() {

      if (executingScrolling) {
        return;
      }

      executingScrolling = true;

      var scroll = $(window).scrollTop();

        if (scroll > 0) {
            if (heroShown) {
              hideHero();
            }
        } else if (!heroShown) {
            backToInit();
        }

      setTimeout(function() {executingScrolling = false;}, 600);
    }

    function initScroll(detlaY, delta) {
      if (detlaY < 0 && $(window).scrollTop() < 2 && !heroShown) {
          backToInit();
        }
        if (detlaY < 0 && heroShown) return;

        // it'll just not do anything while it's false
        if (!doMouseWheel) return;

        $("#heroWrapper").addClass("header-out");
        $(".trigger:not('.scrolltrigger')").addClass("dissapear");

        setTimeout(function() {
          $('body').css({'overflow-y': 'scroll'});
          $('body').css({'position': 'relative'});
          $("#heroWrapper").addClass("header-out-transition");
          doMouseWheel = false;
          heroShown = false;
        }, 1000);
    }

    $("a#herofooterclick").bind("click", function(event, delta) {
          initScroll(100, delta);
    });

    $("a#herohandclick").bind("click", function(event, delta) {
          initScroll(1, delta);
    });

    $("body").bind("wheel", function(event, delta) {
          initScroll(event.originalEvent.deltaY, delta);
          console.log('wheel');
    });

    $("body").bind("DOMMouseScroll", function(event, delta) {
          initScroll(event.originalEvent.deltaY, delta);
          console.log('DOMMouseScroll');
    });

    $("body").bind("scroll", function(event, delta) {
          initScroll(event.originalEvent.deltaY, delta);
          console.log('scroll');
    });

    $("html, body").bind("touchmove", function(event, delta) {
          initScroll(event.originalEvent.deltaY, delta);
          console.log('touchmove');
    });

    $(window).scrollTop(0);

    var styleElement = document.createElement('style');
    styleElement.id = 'remove-scroll-style';
    styleElement.textContent =
        'html::-webkit-scrollbar{display:none !important}' +
        'body::-webkit-scrollbar{display:none !important}';
    document.getElementsByTagName('body')[0].appendChild(styleElement);
  }

  $(document).ready(function(){
    if($('#heroWrapper').size()) {
      ready();
    }
  });

})(window,jQuery);
