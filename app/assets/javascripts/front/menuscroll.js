;(function(window,$,undefined) {

  function ready() {

    var lastScrollTop = 0;

    $(window).on('scroll', function(e) {

      var st = $(this).scrollTop();

      if (st > lastScrollTop){
        $('.floating-nav').addClass('is-hidden');
      } else if($(window).scrollTop() > 200) {
        $('.floating-nav').removeClass('is-hidden');
      } else {
        $('.floating-nav').addClass('is-hidden');
      }

      lastScrollTop = st;

      if ($(this).scrollTop() == 0) {
        $('.floating-nav').addClass('is-hidden');
      }
    });
  }

  $(document).ready(function(){
    if($('nav.floating-nav').size()) {
      ready();
    }
  });

})(window,jQuery);
