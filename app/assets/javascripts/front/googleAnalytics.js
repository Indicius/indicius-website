
window.ga_send_event = function(category, action, label, value) {
  if (window.ga) {
    ga('send', 'event', category, action, label, value);
  }
};

$(document).on('page:change', function() {
  if (window.ga) {
    ga('set', 'location', location.href.split('#')[0]);
    ga('send', 'pageview', { "title": document.title });
  }
});
