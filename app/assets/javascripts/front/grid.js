var $gridElement, $grid;

// Center tweets on load using JS in order to avoid CSS transforms
// (thus causing blurriness on text)

function verticallyCenterTweets() {
  $('.grid .twitter span.tweet').each(function() {
    var tweetNegativeMargin = - $(this).outerHeight()/2;
    $(this).css('margin-top', tweetNegativeMargin);
  });
}

// Center TAG's case study block in grid using JS in order to avoid
// CSS transforms (thus causing blurriness on text)

function centerTagsBlock() {
  var tagsBlock = $('.grid #case-study-block-tag .wrapper');
  var tagsBlockNegativeMarginV = - tagsBlock.outerHeight()/2;
  var tagsBlockNegativeMarginH = - tagsBlock.outerWidth()/2;
  tagsBlock.css({
    'margin-top': (tagsBlockNegativeMarginV - 10) + 'px',
    'margin-left': tagsBlockNegativeMarginH + 'px'
  });
}

$(document).ready(function() {

  // Grid layout using Packery

  $gridElement = $('#theGrid');
  $grid = $gridElement.packery({
    isInitLayout: false,
    columnWidth: '.grid-sizer',
    itemSelector: '.grid-item',
    percentPosition: true,
    transitionDuration: 0,
    gutter: 0
  });

  $grid.packery();

  // Load More

  $('#load-more').on('click', function(e) {
    var $link = $(this);
    if ($link.hasClass('working') || $('#load-more-row').hasClass('done')) return;
    $link.addClass('working');
    var start = $('.grid-item', $gridElement).size()
      + $('.grid-item.case-study', $gridElement).size();

    $.get('/', { start: start }, 'script')
    .done(function(data) {
      prepareNewItems();
      $link.removeClass('working');
      verticallyCenterTweets();
      centerTagsBlock();
    })
    .fail(function() {
      console.error("Grid fetch failed!");
    })
    .always(function() {
    });

    e.preventDefault();
  });

  // Center tweets and TAG's case study block in grid when loading and resizing

  $(window).on('load', function() {
    verticallyCenterTweets();
    centerTagsBlock();
  });

  var resizeTimeout = null;
  $(window).on('resize', function(e) {
    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(function() {
      verticallyCenterTweets();
      centerTagsBlock();
    }, 100);
  });
});
