var formTextSizing = function() {
  var $formText = $('#footer .about');
  var $form = $('#footer .contact');

  if ($(window).width() > 991) {
    $formText.css({
      'min-height': $form.height() + 'px'
    });
  } else {
    $formText.css({
      'min-height': '1px'
    });
  }
};
