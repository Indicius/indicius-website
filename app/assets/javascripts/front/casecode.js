$(document).ready(function() {

  if (body_id = $('body').attr('id')) $(this).trigger('ready:' + body_id.replace(/^case\-study\-page\-/i, ''));
  $('.container.cs-hero').css('height', $(window).height());

  new WOW({
    offset: 200,
    mobile: false
  }).init();

});
