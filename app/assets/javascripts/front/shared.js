window.resetLoading = function () {
  $('.preloader, .preloader .bar, .preloader .bar:after').attr('style', '');
};

window.displayLoading = function (callback) {
  callback = callback || function () {};
  $('.preloader .bar').css('width', '100%');

  setTimeout(function () {
    $('.preloader').fadeOut(300, callback);
    $('.container-trigger').fadeIn();
    $('.logocases').fadeIn();
    $('.menuheroright').fadeIn();
  }, 1200);
};

function onSubmit (token) {
  $('#contact_form').submit();
}

$(window).on('load', function () { formTextSizing(); });
$(window).on('resize', function () { formTextSizing(); });

(function ($, window, undefined) {
  var emptyValidation = function (value) {
    return value.match(/\S+/g) != null;
  };

  var emailValidation = function (value) {
    return value.match(/\S+@\S+(?:\..+)+/g) != null;
  };

  // Check scroll position to show "Go to top" button
  $(window).on('scroll', function () {
    if ($(window).scrollTop() > 600) {
      $('a.go-top').addClass('show');
    } else {
      $('a.go-top').removeClass('show');
    }
  });

  $(document).ready(function () {
    $.loadAsyncImages();
    displayLoading();
    socialOverlay();

    $('.trigger').on('click', function () {
      var scroll = $(window).scrollTop();

      $('.trigger').each(function () {
        $(this).toggleClass('on');
        if (scroll > 0 && $(this).hasClass('on')) $(this).removeClass('dissapear');
      });
      $('.menu').fadeToggle(200);

      if ($(this).hasClass('on')) {
        $('body').bind('mousewheel touchmove', function (e) {
          e.preventDefault();
        });
      } else {
        $('body').unbind('mousewheel touchmove');
      }
    });

    $('button#letstalk').prop('disabled', true);

    var contactForm = $('#contact_form');

    contactForm.on('submit', function (e) {
      // Prevent the default behavior of a form
      e.preventDefault();

      $('#contact_form').addClass('loading');

      // Get the values from the form
      var name = $('#contact_name').val();
      var address = $('#contact_address').val();
      var message = $('#contact_message').val();

      // Our AJAX POST
      $.ajax({
        type: 'POST',
        url: '/contact',
        data: {
          contact: {
            name: name,
            address: address,
            message: message,
            captcha: grecaptcha.getResponse()
          }
        },
        success: function (resp) {
          var $form = $('#contact_form');
          $('.field', $form).removeClass('error');
          $form.removeClass('loading');
          $form.addClass('sent');
          window.ga_send_event('Lead', 'Contact Us', 'Footer');
          $('.field :input', $form).val('');
          $('.field', $form).removeClass('dirty', 'error', 'focus');
        }
      });
    });

    $(document)

      // Scroll to anchor links
      .on('click', 'a[href*="#"]:not([href="#"])', function (e) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') ||
          location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            $('html,body').animate({ scrollTop: target.offset().top}, 2000, 'easeInOutQuart');
            return false;
          }
        }
      })

      // Contact form
      .on('focusout', '#contact_form :input', function (e) {
        var id = $(this).attr('id'),
          $field = $(this).closest('.field'),
          error_ = false,
          validation = (id == 'contact_address' ? emailValidation : emptyValidation);

        if (validation.call(this, $(this).val())) {
          $field
            .removeClass('error')
            .addClass('dirty');

          $('.field').each(function () {
            if ($(this).hasClass('error')) {
              error_ = true;
            }
          });

          if ($('#contact_name').val() == '') {
            error_ = true;
          }

          if ($('#contact_address').val() == '') {
            error_ = true;
          }

          if (error_) {
            $('button#letstalk').prop('disabled', true);
          } else {
            $('button#letstalk').prop('disabled', false);
          }
        } else {
          $field
            .removeClass('dirty')
            .addClass('error');
        }
      })

      .on('focus focusout', '#contact_form :input', function (e) {
        var $field = $(this).closest('.field');
        $field.toggleClass('focus');
      })

      .on('focus', '#contact_form :input', function (e) {
        $('.g-recaptcha').addClass('vis');
      })

      .on('ajax:before', '#contact_form', function (e, data, status, xhr) {
        var $form = $('#contact_form');
        $form.addClass('loading');
      })

      .on('ajax:success', '#contact_form', function (e, data, status, xhr) {
        var $form = $('#contact_form');
        $('.field', $form).removeClass('error');
        $form.removeClass('loading');
        if (!data.success) {
          $.each(data.errors, function (i, field) {
            $('#contact_' + field).closest('.field').addClass('error');
          });
        } else {
          $form.addClass('sent');
          window.ga_send_event('Lead', 'Contact Us', 'Footer');
          $('.field :input', $form).val('');
          $('.field', $form).removeClass('dirty', 'error', 'focus');
        }
      });
    
    if ($('.reactions-container').length > 0) {
      $('.reactions-options-1').hide();
      $('.reactions-second-content').hide();
      $('.reactions-options-2').hide();
      $('.reactions-options-3').hide();

      $reactionButton = $('.reactions-container .emoji-btn');

      $reactionButton.on('click', function(e) {
        e.preventDefault();

        $reactionButton.removeClass('is-active');

        $(this).addClass('is-active');
        $(this).parents('.reactions-container').addClass('is-selected');

        $('.reactions-options-all').slideUp(300);

        /* If user clicked on optio 1 */
        if ($(this).hasClass('option-1')) {
          $('.reactions-options-1').slideDown(300);
        } else if ($(this).hasClass('option-2')) { 
          /* If user clicked on option 2 */
          $('.reactions-options-2').slideDown(300);

        } else { ($(this).hasClass('option-3'))
          /* In this case, is option 3 */
          $('.reactions-options-3').slideDown(300);
        }
      });

      /* When user selects an option inside Options 1 (Love it!) */
      $buttonsOption1 = $('.reactions-options-1 .btn-option');
      $buttonFeedback = $('.send-feedback-btn');

      $buttonFeedback.on('click', function (e){
        e.preventDefault();

        $buttonFeedback.removeClass('is-selected');

        $(this).addClass('is-selected');

        confetti.start();

        setTimeout(function() {
          confetti.stop();
        }, 1500);

        $('.reactions-main-content').slideUp(300);
        $('.reactions-second-content').slideDown(300);

        if ($(this).hasClass('option-good')) {
          var feedback = $('.input-option-good').val();
          window.ga_send_event('Case Study - Feedback', 'Good', feedback);

        } else if ($(this).hasClass('option-meh')) {
          var feedback = $('.input-option-meh').val();
          window.ga_send_event('Case Study - Feedback', 'Meh', feedback);
        }
      });

      $buttonsOption1.on('click', function(e) {
        e.preventDefault();
        var reactionsContainerTopPosition = $('.reactions-container').offset().top + $(window).height() / 2;

        $buttonsOption1.removeClass('is-selected');

        $(this).addClass('is-selected');

        var feedback = $(this).text();

        window.ga_send_event('Case Study - Feedback', 'Love it!', feedback);

        confetti.start();

        setTimeout(function() {
          confetti.stop();
        }, 1500);

        $('.reactions-main-content').slideUp(300);
        $('.reactions-second-content').slideDown(300);
      });

    }
  });
})(jQuery, window);
