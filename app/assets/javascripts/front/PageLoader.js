function PageLoader(args) {
  this.o = $.extend(false, args, {
    defaultColor: $('body').css('background-color'),
    startTime: Date.now()
  });
  this.init();
}

PageLoader.prototype.init = function() {
  this.setupTransition();
  this.loadPage();
};

PageLoader.prototype.setupTransition = function() {
  $('#transition-color').css('background', this.o.state.data.color || this.o.defaultColor);
  $('body').addClass('transitioning');
};

PageLoader.prototype.loadPage = function() {
  var self = this;
  $.ajax({
    url: this.o.state.url,
    beforeSend: function() {
    }
  })
  .fail(function(data) {
    console.log('Failed to load!');
  })
  .done(function(data) {
    self.setupPage(data);
  });
};

PageLoader.prototype.setupPage = function(data) {
  var self = this;
  if ( content = self.grabContent(data) ) {
    var title = self.grabTitle(data);
    var id = self.grabBodyId(data);
    setTimeout(function() {
      $('#page').html(content);
      $('.menu_scroll_loader').html(content);
      $('title').text(title);
      setTimeout(function() {
        if ( id )
          $('body').attr('id', id);
        $(document).trigger('pageLoaded');
        setTimeout(function() {
          $('#transition-color').css('background', '');
        }, 100);
      }, -1)
    }, (self.o.animationTotalTime - (Date.now() - self.o.startTime)));
  }
};

PageLoader.prototype.grabContent = function(data) {
  var el = $(data).filter('#page');
  if ( el.size() )
    return el.html();
  return null;
};

PageLoader.prototype.grabBodyId = function(data) {
  var matches = data.match(/<body (?:id="([^"]+)").*>/i);
  if ( matches && matches[1] )
    return matches[1];
  return null;
};

PageLoader.prototype.grabBodyClasses = function(data) {
  var matches = data.match(/<body.*(?:class="([^"]+)").*>/i);
  if ( matches && matches[1] )
    return matches[1];
  return null;
};

PageLoader.prototype.grabTitle = function(data) {
  var matches = data.match(/<title>([^<]+)<\/title>/i);
  if ( matches && matches[1] )
    return matches[1];
  return '';
};
