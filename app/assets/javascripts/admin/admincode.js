var slug = function(str) {
  var $slug = '';
  var trimmed = $.trim(str);
  $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
  replace(/-+/g, '-').
  replace(/^-|-$/g, '');
  return $slug.toLowerCase();
}

$(document).ready(function() {

  $('#case_study_title').on('ready change keyup', function(e) {
    $('#generate-slug').toggleClass('disabled', ($(this).val() === ''));
  }).trigger('ready');

  $('#generate-slug').on('click', function(e) {
    if ( $(this).hasClass('disabled') )
      return;
    $('#case_study_slug').val(slug($('#case_study_title').val()));
  });

});
