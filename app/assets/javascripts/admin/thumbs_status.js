$('#item-thumbs-container .item').on('ajax:beforeSend', function(event, xhr, settings) {
  if ($(this).hasClass('loading')) {
    return false;
  }
  $(this).addClass('loading');
});
