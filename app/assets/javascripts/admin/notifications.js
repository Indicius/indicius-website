window.notify = function(type, msg) {
  noty({
    text: msg,
    layout: 'notification',
    theme: 'indicius',
    type: type,
    timeout: 5000,
    animation: {
      open: 'animated bounceInRight',
      close: 'animated fadeOutRight'
    }
  });
}

$(document).ready(function() {
  $('input[type="hidden"][data-notification]').each(function(index, el){
    window.notify($(el).attr('data-notification'), $(el).val());
    $(el).remove();
  });
});