module Admin
  module GridHelper

    def calculate_item_position(page_number, item_index)
      page_number * GRID[:page_size] + item_index
    end

    def edit_link_to(path)
      link_to 'Edit', path
    end

    def delete_link_to(path)
      link_to 'Delete', path, data: { confirm: 'Are you sure?' }, method: :delete
    end

    def data_attributes item
      attributes = {
        id: item.id,
        type: item.class.to_s.underscore
      }
      attributes.merge!({ CaseStudy::STYLES.key(item.block_style) => 1 }) if attributes[:type] == 'case_study'
      attributes
    end

    def type_class item
      "type-#{item.class.to_s.underscore}"
    end

  end
end
