module CaseStudiesHelper

  def case_study_page_title
    "Case Study: #{@case_study.title}"
  end

  def render_linked(case_study, side)
    render(
      partial: case_study.link_template_path,
      locals: { presenter: case_study, side: side }
    )
  end

  def render_left_linked
    return unless @case_study.left_link.present?
    render_linked(@case_study.left_link, :left)
  end

  def render_right_linked
    return unless @case_study.right_link.present?
    render_linked(@case_study.right_link, :right)
  end

end
