module ApplicationHelper

  def present(model)
    return '' if model.nil?
    presenter = presenter_class(model).new(model, self)
    if block_given?
      yield(presenter)
    else
      presenter.render
    end
  end

  def body_id
    if @case_study && @case_study.page_template
      "case-study-page-#{@case_study.page_template}"
    else
      'index'
    end
  end

  def inline_svg(filename)
    File.open(Rails.root.join('app', 'assets', 'images', filename), "rb") do |file|
      raw file.read
    end
  end

  def image_srcset_tag(source, srcset = {}, options = {})
    srcset = srcset.map { |size, src| "#{image_path(src)} #{size}" }.join(', ')
    image_tag(image_path(source), options.merge(srcset: srcset))
  end

  private

  def presenter_class(model)
    presenter_file("#{model.class}Presenter").constantize
  end

  def presenter_file(name)
    return name if File.exist? Rails.root.join('app', 'presenters', "#{name.underscore}.rb")
    'BasePresenter'
  end

end
