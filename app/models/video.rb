class Video < ActiveRecord::Base

  scope :latest, -> { order('timestamp DESC').first }
  validates :video_id, :url, :timestamp, presence: true
  validates :video_id, uniqueness: true

  def self.cached_videos(starting_position, last_video, amount)
    Rails.cache.fetch("#{name.underscore}__start_#{starting_position}") do
      where('timestamp < ?', last_video).order('timestamp DESC').limit(amount).to_a
    end
  end

end
