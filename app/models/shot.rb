class Shot < ActiveRecord::Base
  scope :latest, -> { order('timestamp DESC').first }
  scope :valid_shots, -> { where(visible: true, processed: true) }
  scope :active, -> { where("scheduled_for <= ? OR scheduled_for IS NULL", Time.zone.now) }

  def self.media_path(lead='/')
    File.join(lead.to_s, 'media', 'dribbble')
  end

  def media_path
    self.class.media_path
  end

  def static_image
    file = shot_id.to_s
    file += "_f" if is_gif
    file += ".#{file_type}"
    File.join(media_path, file)
  end

  def static_image_hd
    file = shot_id.to_s
    file += "_f" if is_gif
    file += "@2x.#{file_type}"
    File.join(media_path, file)
  end

  def animated_image
    if is_gif
      File.join(media_path, "#{shot_id}.gif")
    end
  end

  def self.cached_shots(starting_position, last_shot, amount)
    Rails.cache.fetch("#{name.underscore}__start_#{starting_position}") do
      active.valid_shots.where('timestamp < ?', last_shot).order('timestamp DESC').limit(amount).to_a
    end
  end

  def self.cached_shots_left(starting_position, last_shot)
    Rails.cache.fetch("#{name.underscore}__left_start_#{starting_position}") do
      active.valid_shots.where('timestamp < ?', last_shot).limit(GRID[:page_size]).size
    end
  end
end
