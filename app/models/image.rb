class Image < ActiveRecord::Base

  has_one :grid_item, as: :gridable, dependent: :destroy
  after_commit :self_clear_cache

  mount_uploader :grid_image, GridImageUploader

  def self_clear_cache
    GridItem.clear_cache
  end
end
