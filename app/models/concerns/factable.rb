module Factable
  extend ActiveSupport::Concern

  included do
    self.table_name = 'facts'
    serialize :data
    validates :block_type, uniqueness: true
    has_one :grid_item, as: :gridable
  end

end
