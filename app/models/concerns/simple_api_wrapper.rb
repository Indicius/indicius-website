module SimpleApiWrapper
  extend ActiveSupport::Concern
  module ClassMethods

    private

    def build_url(*parts)
      ([class_variable_get(:@@base_url)] + parts).join('/')
    end

    def grab_contents(url, params={})
      open("#{url}?#{class_variable_get(:@@default_params).merge(params).to_param}").read
    rescue
      ''
    end

    def grab_contents_with_headers(url, headers, params={})
      open("#{url}?#{class_variable_get(:@@default_params).merge(params).to_param}", headers).read
    rescue
      ''
    end

    def parse_contents(raw)
      JSON.parse(raw)
    rescue
      nil
    end

    def request_remote_data(url, params={})
      cached_output("#{url.parameterize}.bin") do
        parse_contents(grab_contents(url, params))
      end
    end

    def request_remote_data_with_headers(url, headers, params={})
      cached_output("#{url.parameterize}.bin") do
        content = grab_contents_with_headers(url, headers, params)
        dest = Rails.root.join('tmp', "page_#{params[:page]}.json")
        File.open(dest, 'wb+') { |f| f.puts content }
        parse_contents(content)
      end
    end

  end
end
