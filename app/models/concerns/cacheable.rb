module Cacheable
  extend ActiveSupport::Concern
  module ClassMethods

    def cached_output(file, &_data)
      cache_file = File.join(Rails.root, 'cache', file)
      if class_variable_get(:@@use_cache)
        return restore(file) if File.exist?(cache_file)
        result = yield
        store(file, result)
        return result
      end
      yield
    end

    private

    def store(file, data)
      File.open(File.join(Rails.root, 'cache', file), 'wb') { |f| f.write(Marshal.dump(data)) }
    end

    def restore(file)
      m = nil
      File.open(File.join(Rails.root, 'cache', file), 'rb') { |f| m = Marshal.load(f) }
      m
    end

  end
end
