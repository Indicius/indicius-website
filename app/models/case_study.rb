class CaseStudy < ActiveRecord::Base

  scope :linkeable, -> { where.not(link_template: nil) }
  scope :excluding, -> (case_study) { case_study ? where.not(id: case_study.id) : where({}) }

  has_one :grid_item,
    as: :gridable,
    dependent: :destroy

  belongs_to :left_link,
    class_name: 'CaseStudy',
    foreign_key: 'left_link_id'

  belongs_to :right_link,
    class_name: 'CaseStudy',
    foreign_key: 'right_link_id'

  has_one :left_linked,
    class_name: 'CaseStudy',
    foreign_key: 'left_link_id',
    dependent: :nullify

  has_one :right_linked,
    class_name: 'CaseStudy',
    foreign_key: 'right_link_id',
    dependent: :nullify

  STYLES = {
    vertical: 1,
    horizontal: 2
  }

  validates :title, :slug, :page_template, :block_template, :link_template, :block_style,
    presence: true

  validates :slug,
    uniqueness: true

  validates :block_style,
    inclusion: { in: STYLES.values }


  def link_template_path
    "case_studies/links/#{link_template}"
  end

end
