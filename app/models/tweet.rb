class Tweet < ActiveRecord::Base

  scope :latest, -> { order('timestamp DESC').first }
  has_one :grid_item, as: :gridable

  validates :tweet_id, :text, :timestamp, presence: true
  validates :tweet_id, uniqueness: true

  def url
    "https://twitter.com/Indicius/status/#{tweet_id}"
  end

  def self.ids_for_last(number)
    order('timestamp DESC').limit(number).pluck(:id)
  end

  def self.update_positions
    current_ids = GridItem.ids_for_grided_type 'Tweet'
    return unless current_ids.any?
    new_tweets_ids = ids_for_last current_ids.size
    current_ids.each do |current_id|
      GridItem.update_grided_id_for current_id, new_tweets_ids.shift
    end
  end

  def self.limit_reached?
    GridItem.where(gridable_type: 'Tweet').count >= count
  end

end
