class DribbbleFact < ActiveRecord::Base

  include Factable
  default_scope { where(block_type: :dribbble) }

end
