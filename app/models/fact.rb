class Fact < ActiveRecord::Base
  scope :of_type, ->(type) { find_by(block_type: type) }
end
