class FacebookFact < ActiveRecord::Base

  include Factable
  default_scope { where(block_type: :facebook) }

end
