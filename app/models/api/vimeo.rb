require 'open-uri'
require 'json'

module Api
  class Vimeo

    @@use_cache = false
    @@base_url = 'https://api.vimeo.com'
    @@default_params = {
      page: 1,
      per_page: 15,
      sort: 'date',
      direction: 'desc'
    }

    include Cacheable
    include SimpleApiWrapper

    def self.videos(params={})
      request_remote_data_with_headers(
        build_url('users/indicius/videos'),
        { 'Authorization' => "bearer #{Rails.application.credentials.vimeo[:access_token]}" },
        params
      )
    end

  end
end
