require 'open-uri'
require 'json'

module Api
  class Dribbble

    @@use_cache = false
    @@base_url = 'https://api.dribbble.com/v2'
    @@default_params = {
      access_token: Rails.application.credentials.dribbble[:access_token]
    }

    include Cacheable
    include SimpleApiWrapper

    def self.team(params={})
      request_remote_data(build_url('user'), {}.merge(params))
    end

    def self.shots(params={})
      request_remote_data(build_url('user/shots'), {
        page: 1,
        sort: 'recent',
        per_page: 100
      }.merge(params))
    end

    def self.shot(id)
      request_remote_data(build_url('shots', id))
    end

    def self.attachments(id)
      request_remote_data(build_url("shots/#{id}/attachments"))
    end

  end
end
