module Api
  class Twitter

    @default_params = {
      count: 25,
      include_rts: false,
      exclude_replies: false,
      contributor_details: false,
      trim_user: true
    }

    def self.tweets(params={})
      client = client_instance
      # p @default_params.merge(params)
      client.user_timeline('indicius', @default_params.merge(params))
    rescue
      nil
    end

    def self.amount_to_keep
      @default_params[:count]
    end

    def self.client_instance
      ::Twitter::REST::Client.new do |config|
        config.consumer_key        = Rails.application.credentials.twitter[:consumer][:key]
        config.consumer_secret     = Rails.application.credentials.twitter[:consumer][:secret]
        config.access_token        = Rails.application.credentials.twitter[:access_token]
        config.access_token_secret = Rails.application.credentials.twitter[:access_token_secret]
      end
    end

  end
end
