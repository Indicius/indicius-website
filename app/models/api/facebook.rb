require 'open-uri'
require 'json'

module Api
  class Facebook

    @@use_cache = false
    @@base_url = 'https://graph.facebook.com/v2.10'
    @@default_params = {
      access_token: Rails.application.credentials.facebook[:access_token],
      format: 'json',
      method: 'get',
      pretty: 0,
      suppress_http_code: 1
    }

    include Cacheable
    include SimpleApiWrapper

    def self.page
      request_remote_data(build_url('117509834997258'), { fields: 'id,name,fan_count,link' })
    end

  end
end
