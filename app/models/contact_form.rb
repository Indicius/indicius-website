class ContactForm
  include ActiveModel::Model

  attr_accessor :name, :address, :message, :verification

  validates :name, presence: true
  validates :address, presence: true
  validates :message, presence: true
  validates :verification, absence: true
end
