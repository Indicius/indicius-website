class GridItem < ActiveRecord::Base

  belongs_to :gridable, polymorphic: true
  default_scope { order(position: :asc) }

  validates :position, presence: true, uniqueness: true
  after_commit :self_clear_cache

  def self_clear_cache
    self.class.clear_cache
  end

  def self.ids_for_grided_type(grided_type)
    where(gridable_type: grided_type).pluck(:id)
  end

  def self.update_grided_id_for(id, gridable_id)
    unscoped.where('id = ?', id).update_all(gridable_id: gridable_id)
  end

  def self.cached_items(starting_position)
    Rails.cache.fetch("#{name.underscore}__start_#{starting_position}") do
      where(
        'position >= :range_start AND position < :range_end',
        range_start: starting_position,
        range_end: starting_position + GRID[:page_size]
      ).includes(:gridable).to_a
    end
  end

  def self.clear_cache
    Rails.cache.delete_matched("shot__*")
    Rails.cache.delete_matched("video__*")
    Rails.cache.delete_matched("grid_item__*")
  end

end
