$.noty.layouts.notification = {
  name     : 'notification',
    options  : { // overrides options
    },
    container: {
      object  : '<ul id="noty_notification_layout_container" />',
      selector: 'ul#noty_notification_layout_container',
      style   : function() {
        $(this).css({
          top          : 60,
          right        : 20,
          position     : 'fixed',
          width        : '310px',
          height       : 'auto',
          margin       : 0,
          padding      : 0,
          listStyleType: 'none',
          zIndex       : 10000000
        });
      }
    },
    parent   : {
      object  : '<li />',
      selector: 'li',
      css     : {}
    },
    css      : {
      display: 'none',
      width  : '310px'
    },
    addClass : ''
  };