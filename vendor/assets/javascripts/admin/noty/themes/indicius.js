$.noty.themes.indicius = {
  name    : 'indicius',
  helpers : {},
  modal   : {},
  style   : function() {

    this.$bar.css({
      overflow      : 'hidden',
      margin        : '0 0 8px 0',
      borderRadius  : '5px',
      height        : 'auto',
      display: 'inline-block'
    });

    this.$message.css({
      fontSize  : '14px',
      lineHeight: '16px',
      textAlign : 'left',
      padding   : '10px',
      width     : 'auto',
      position  : 'relative'
    });

    this.$bar.on({
      mouseenter: function() {
        $(this).find('.noty_close').stop().fadeTo('normal', 1);
      },
      mouseleave: function() {
        $(this).find('.noty_close').stop().fadeTo('normal', 0);
      }
    });

    switch(this.options.layout.name) {
      default:
      break;
    }

    switch(this.options.type) {
      case 'error':
      this.$bar.css({backgroundColor: '#e74c3c', color: '#ffffff'});
      break;
      case 'notice':
      this.$bar.css({backgroundColor: '#57B7E2', color: '#FFF'});
      break;
      case 'success':
      this.$bar.css({backgroundColor: '#2ecc71', color: '#ffffff'});
      break;
      default:
      this.$bar.css({backgroundColor: '#FFF', borderColor: '#CCC', color: '#444'});
      break;
    }
  },
  callback: {
    onShow : function() {},
    onClose: function() {}
  }
};
