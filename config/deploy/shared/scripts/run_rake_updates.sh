#!/bin/bash

RAILS_ENV='production'

echo -e "== Site Data Update: $(date) ==";
source /home/deployer/.rvm/scripts/rvm;
cd /home/deployer/indicius-2015;

echo -e "\nUpdating Dribble Stats...";
RAILS_ENV=$RAILS_ENV bundle exec rake data:dribbble:update_facts;

echo -e "\nUpdating Dribble Shots...";
RAILS_ENV=$RAILS_ENV bundle exec rake data:dribbble:update_shots;

echo -e "\nProcessing Dribble Shots...";
RAILS_ENV=$RAILS_ENV bundle exec rake data:dribbble:process_images;

echo -e "\nUpdating Facebook Stats...";
RAILS_ENV=$RAILS_ENV bundle exec rake data:facebook:update_facts;

echo -e "\nUpdating Tweets...";
RAILS_ENV=$RAILS_ENV bundle exec rake data:twitter:update_tweets;

echo -e "Updating Vimeo Videos...";
RAILS_ENV=$RAILS_ENV bundle exec rake data:vimeo:update_videos;

echo -e "\nClearing Cache...";
RAILS_ENV=$RAILS_ENV bundle exec rake tmp:cache:clear;

echo -e "\n===============================\n";
