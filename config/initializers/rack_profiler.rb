if Rails.env.development?
  require 'rack-mini-profiler'
  Rack::MiniProfilerRails.initialize!(Rails.application)

  #defaults to Rack::MiniProfiler::FileStore
  Rack::MiniProfiler.config.storage = Rack::MiniProfiler::MemoryStore

  # defaults to true
  Rack::MiniProfiler.config.disable_caching = false
end
