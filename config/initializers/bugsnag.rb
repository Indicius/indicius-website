unless Rails.env.development?
  Bugsnag.configure do |config|
    config.api_key = Rails.application.credentials.bugsnag[:api_key]
    config.send_environment = true
  end
end
