Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  constraints(host: /^www\./i) do
    get '(*any)', to: redirect { |params, request|
      URI.parse(request.url).tap { |uri| uri.host.sub!(/^www\./i, '') }.to_s
    }
  end

  match "/404" => "errors#not_found", via: [:all]

  root 'grid#index'
  post '/contact' => 'contact#validate_and_send', defaults: { format: :json }, as: 'contact_send'
  get 'case-study(/:slug)' => 'case_studies#index', as: 'case_study'

  #get '/careers' => 'careers#listing', via: [:all]
  get '/lets-talk' => 'lets_talk#listing', via: [:all]
  get '/case-studies' => 'case_studies#listing', via: [:all]
  get '/agency' => 'agency#listing', via: [:all]
  get '/what-we-do' => 'what_we_do#listing', via: [:all]
  get '/who-we-are' => 'who_we_are#listing', via: [:all]


  namespace :admin do
    get  '/' => 'dashboard#index', as: 'dashboard'
    get  '/credentials-test' => 'credentials_test#index', as: 'credentials_test'

    get  '/list' => 'list#index', as: 'list'
    get  '/list/add_page/:page_number' => 'list#add_page'

    get  '/grid' => 'grid#index', as: 'grid'
    get  '/grid/add_page/:page_number' => 'grid#add_page'

    resources :images, only: [:edit, :update, :destroy] do
      new do
        get ':position' => 'images#new', as: ''
      end
      collection do
        post ':position' => 'images#create', as: ''
      end
      member do
        get 'update_position/:position' => 'images#update_position', as: 'reposition'
      end
    end

    resources :case_studies, only: [:edit, :update, :destroy] do
      new do
        get ':style/:position' => 'case_studies#new', as: ''
      end
      collection do
        post ':style/:position' => 'case_studies#create', as: ''
      end
      member do
        get 'update_position/:position' => 'case_studies#update_position', as: 'reposition'
      end
    end

    get 'shots' => 'shots#index'
    get 'shots/status/:id' => 'shots#change_status', as: 'shot_status'

    get '/tweets/add/:position' => 'tweets#add', as: 'tweet_add'
    get '/tweets/remove/:position' => 'tweets#remove', as: 'tweet_remove'
    get '/tweets/update_position/:position/:new_position' => 'tweets#update_position', as: 'tweet_reposition'

    get '/facts/add/:type/:position' => 'facts#add', as: 'fact_add'
    get '/facts/remove/:position' => 'facts#remove', as: 'fact_remove'

    get  '/login'   => 'sessions#new'
    post '/login'   => 'sessions#create'
    get  '/logout'  => 'sessions#destroy'

  end
end
