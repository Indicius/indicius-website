# Admin

Se puede acceder al admin en `/admin` usando los siguientes datos:

- **User:** indicius
- **Password:** *1nd12015YAY



# Staging Login

En la instancia de staging existe un HTTP login para prevenir acceso de terceros:

- **User:** indicius
- **Password:** rabas

# Database management

- **DB User:** root
- **DB Password:** DTwhogmyBLbCj7uodGQ8

# Deployments 

Actualmente hay dos servidores: staging y producción.

- **Server staging:** *staging.indicius.com* ó *54.189.216.46*
- **Server producción:** *indicius.com* ó *54.218.144.191*

Para conectarse a los servidores desde un sistema UNIX correr el siguiente comando:

- `ssh -i "~/Certificates/indicius.pem" [IP del servidor deseado]` (asumiendo que la key *indicius.pem* se encuentra en el directorio `~/Certificates/`. En caso de no tener la key, pedírsela a un administrador de Indicius)
- Una vez que se ingresó al server, correr: `rm /var/www/indicius/shared/last_revision`
- Luego correr el script de deploy usando: `./run_deploy.sh`
- Este proceso hará un deploy con el contenido de master del [repositorio de Indicius](https://bitbucket.org/indicius/indicius-2015)

# Front-end general

Los archivos principales utilizados para el estilado y funcionamiento son los siguientes:

Layout del index del front-end: `app/views/application.html.slim`.

Markup del index del front-end: `app/views/grid/index.html.slim`.

Markup de la subsección de case studies del front-end: `app/views/case_studies/listing.html.slim`

CSS principal del front-end: `app/assets/stylesheets/application.css.scss`.

Media queries del front-end: `app/assets/stylesheets/front/media-queries.scss`.

JS principal del front-end: `app/assets/javascripts/application.js`.

JS principal de los case studies: `app/assets/javascripts/case-studies.js`.


# Case Studies

Los case studies están compuestos por 3 templates y es necesario contar con los primeros 2 archivos para dar de alta un nuevo case study.

Template | Namespace
-------- | ---------
**Page** | Se utiliza para la página de cada case study. Comparte el layout con el resto de los case studies. Tiene Javascript propio.
**Block** | Se utiliza para mostrar el case study en la grilla.
**Listing** | Se utiliza para mostrar la grilla de la subsección de case studies 

Los archivos a tener en cuenta para editar/crear un case study son:

```
app
├── assets
│   ├── javascripts
│   │   ├── case_studies
│   │   │   └── test.js
│   │   └── case-studies.js
│   └── stylesheets
│       ├── case_studies
│       │   └── test.scss
│       └── application.css.scss
└── views
    ├── case_studies
    │   ├── blocks
    │   │   └── _test.slim
    │   └── pages
    │       └── test.html.slim
    └── layouts
        └── case-studies.html.slim
```

Los archivos dentro de `app/views/case_studies/blocks/` son el markup para los bloques de los case studies dentro de la grilla del home del sitio.

Los archivos dentro de `app/views/case_studies/pages/` son el markup para el case study.

Los archivos dentro de `app/assets/javascripts/case_studies/` son el JS para el case study.

Los archivos dentro de `app/assets/stylesheets/case_studies/` son el CSS para el case study.


**Es importante que los nombres de los archivos concuerden, ya que hay varios checkeos en el backend que se basan en esto para tomar desiciones.**


## Namespacing

Es posible aplicar diferentes estilos y scripts a cada case study siguiendo los siguientes patrones:

Template | Namespace
-------- | ---------
**Page** | El body se genera con ID: `case-study-page-<page_template>`
**Block** | El contenedor del bloque se genera con ID: `case-study-block-<block_template>`


## Styles

Para optimizar recursos y bajar la complejidad de uso se optó por que los estilos de los case studies formen parte del archivo `application.css.scss`. Utilizando namespacing y un nesting razonable esto debería ser simple de usar.

Todos los estilos referidos a un case study van dentro de un partial de SASS dentro de la sub carpeta `case_studies`, que debe ser importado al final de `application.css.scss`:

```
@import "case_studies/<page_template>";
```


## Scripts

A diferencia de los estilos, el namespacing en caso de los scripts está mucho mas simplificado. Se puede ejecutar código arbitrario dependiendo del ID del body utilizando un evento con namespace que siempre está disponible.

```
$(document).on('ready:<page_template>', function() {
  // Javascript para este case study acá.
});
```

Si se necesitan incluir librerías se lo puede hacer directamente desde el archivo JS de cada case study `case_studies/<page_template>.js`

Adicionalmente, si hay una librería compartida por todos los case studies se puede incluir directamente en el archivo `case-studies.js`.


# Mini Profiler

En la parte superior izquierda de la pantalla aparece el accesso a mini profiler, que es una herramienta que estamos usando para medir la performance de la app.

Es posible ocultar/mostrar el profiler con `ALT` + `P`.


# Info & Resources

### Polymorphic / has_one

[http://guides.rubyonrails.org/association_basics.html#methods-added-by-has-one]()

### CDN / Assets host

[http://www.happybearsoftware.com/use-cloudfront-and-the-rails-asset-pipeline-to-speed-up-your-app.html]()

### Automated Tasks with Cron and Rake

[http://tutorials.jumpstartlab.com/topics/systems/automation.html]()

### Rolling Restart UNICORN

[http://vladigleba.com/blog/2014/03/21/deploying-rails-apps-part-3-configuring-unicorn/]()

# Deprecated

## Deployments (WIP)

Hay 2 rake tasks armadas para hacer deploys, una para el servidor de **production** y otra para el de **staging**.

- `rake deploy:production`
- `rake deploy:staging`

Para correrlas es necesario tener la keys (public y private) y configurar ssh para usarlas al conectarse ambos servers, que por el momento usan las mismas keys (pero esto puede cambiar)

Las keys tienen que estar en los directorios que figuran a continuación, que se tienen que crear a mano:

- `~/.ssh/ids/indicius_production_do`
- `~/.ssh/ids/indicius_staging_do`

Es importante que se asignen bien los permisos para los directorios y las keys para que SSH las reconozca:

Tipo | Permisos
---- | --------
Directorio | 700
Archivo Key | 600

Finalmente hay que modificar el ssh config que se encuentra en `~/.ssh/config` lo siguiente. Si no existe hay que crearlo:

```
Host indicius_production_do
	HostName 104.236.56.77
	Port 2222
	IdentityFile ~/.ssh/ids/indicius_production_do/id_rsa
	IdentitiesOnly yes

Host indicius_staging_do
	HostName ???
	Port 2222
	IdentityFile ~/.ssh/ids/indicius_staging_do/id_rsa
	IdentitiesOnly yes
```
