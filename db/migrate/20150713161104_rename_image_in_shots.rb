class RenameImageInShots < ActiveRecord::Migration
  def change
    rename_column :shots, :image, :remote_image
  end
end
