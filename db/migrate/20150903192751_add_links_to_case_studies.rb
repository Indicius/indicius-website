class AddLinksToCaseStudies < ActiveRecord::Migration
  def change
    add_column :case_studies, :left_link_id, :integer, after: :block_style
    add_column :case_studies, :right_link_id, :integer, after: :left_link_id
  end
end
