class ArrangeCaseStudiesColumns < ActiveRecord::Migration
  def change
    change_column :case_studies, :title, :string, after: :id
  end
end
