class RemoveScreenNameFromTweets < ActiveRecord::Migration
  def change
    remove_column :tweets, :screen_name, :string
  end
end
