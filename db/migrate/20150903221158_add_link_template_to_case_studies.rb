class AddLinkTemplateToCaseStudies < ActiveRecord::Migration
  def change
    add_column :case_studies, :link_template, :string, after: :block_template
  end
end
