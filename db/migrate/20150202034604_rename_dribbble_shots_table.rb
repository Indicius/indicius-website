class RenameDribbbleShotsTable < ActiveRecord::Migration
  def change
    rename_table :dribbble_shots, :shots
  end
end
