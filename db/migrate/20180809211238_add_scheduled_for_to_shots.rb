class AddScheduledForToShots < ActiveRecord::Migration
  def change
    add_column :shots, :scheduled_for, :datetime
  end
end
