class AddLinkToImages < ActiveRecord::Migration
  def change
    add_column :images, :link, :string, after: :grid_image
  end
end
