class AddIsHdToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :is_hd, :boolean, default: true
  end
end
