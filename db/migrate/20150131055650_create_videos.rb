class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.integer :video_id, limit: 8
      t.string :image
      t.string :url
      t.integer :timestamp
    end
  end
end
