class ChangeTweetsTablesEncodingForEmojis < ActiveRecord::Migration
  def change
    execute "ALTER TABLE tweets CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"
    execute "ALTER TABLE tweets MODIFY COLUMN text VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"
  end
end
