class AddTitleToCaseStudies < ActiveRecord::Migration
  def change
    add_column :case_studies, :title, :string
  end
end
