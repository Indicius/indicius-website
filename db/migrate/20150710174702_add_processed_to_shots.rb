class AddProcessedToShots < ActiveRecord::Migration
  def change
    add_column :shots, :processed, :boolean, default: false
  end
end
