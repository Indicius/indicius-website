class CreateCaseStudies < ActiveRecord::Migration
  def change
    create_table :case_studies do |t|
      t.string :slug
      t.string :page_template
      t.string :block_template
      t.integer :block_style
    end
  end
end
