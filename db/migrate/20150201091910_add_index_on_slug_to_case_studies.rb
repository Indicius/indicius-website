class AddIndexOnSlugToCaseStudies < ActiveRecord::Migration
  def change
    add_index :case_studies, :slug
  end
end
