class AddIsHdToShots < ActiveRecord::Migration
  def change
    add_column :shots, :is_hd, :boolean, default: true
  end
end
