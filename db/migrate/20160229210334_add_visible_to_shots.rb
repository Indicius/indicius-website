class AddVisibleToShots < ActiveRecord::Migration
  def change
    add_column :shots, :visible, :boolean, default: true
  end
end
