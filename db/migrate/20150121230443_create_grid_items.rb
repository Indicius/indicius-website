class CreateGridItems < ActiveRecord::Migration
  def change
    create_table :grid_items do |t|
      t.integer :position
      t.belongs_to :gridable, polymorphic: true
    end
    add_index :grid_items, [:gridable_id, :gridable_type]
  end
end
