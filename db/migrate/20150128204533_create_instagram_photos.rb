class CreateInstagramPhotos < ActiveRecord::Migration
  def change
    create_table :instagram_photos do |t|
      t.integer :photo_id
      t.string :text
      t.string :image
      t.string :url
      t.integer :timestamp
    end
  end
end
