class AddRemoteImageHdToShots < ActiveRecord::Migration[5.2]
  def change
    add_column :shots, :remote_image_hd, :string
  end
end
