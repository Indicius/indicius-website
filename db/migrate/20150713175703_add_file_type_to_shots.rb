class AddFileTypeToShots < ActiveRecord::Migration
  def change
    add_column :shots, :file_type, :string, after: :remote_image
  end
end
