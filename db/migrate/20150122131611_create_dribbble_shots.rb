class CreateDribbbleShots < ActiveRecord::Migration
  def change
    create_table :dribbble_shots do |t|
      t.integer :shot_id
      t.string :image
      t.string :url
      t.integer :timestamp
    end
  end
end
