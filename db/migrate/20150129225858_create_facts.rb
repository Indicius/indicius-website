class CreateFacts < ActiveRecord::Migration
  def change
    create_table :facts do |t|
      t.string :block_type
      t.text :data
    end
  end
end
