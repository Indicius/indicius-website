class ChangeApiBasedIdsToBigint < ActiveRecord::Migration
  def change
    change_column :dribbble_shots, :shot_id, 'bigint'
    change_column :tweets, :tweet_id, 'bigint'
  end
end
