class CreateTweets < ActiveRecord::Migration
  def change
    create_table :tweets do |t|
      t.integer :tweet_id
      t.string :text
      t.string :screen_name
      t.integer :timestamp
    end
  end
end
