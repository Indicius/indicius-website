class AddIsGifToShots < ActiveRecord::Migration
  def change
    add_column :shots, :is_gif, :boolean, default: false, after: :is_hd
  end
end
