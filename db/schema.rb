# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_01_154055) do

  create_table "case_studies", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.string "page_template"
    t.string "block_template"
    t.string "link_template"
    t.integer "block_style"
    t.integer "left_link_id"
    t.integer "right_link_id"
    t.index ["slug"], name: "index_case_studies_on_slug"
  end

  create_table "facts", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.string "block_type"
    t.text "data"
  end

  create_table "grid_items", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.integer "position"
    t.string "gridable_type"
    t.integer "gridable_id"
    t.index ["gridable_id", "gridable_type"], name: "index_grid_items_on_gridable_id_and_gridable_type"
  end

  create_table "images", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.string "grid_image"
    t.string "link"
  end

  create_table "instagram_photos", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.integer "photo_id"
    t.string "text"
    t.string "image"
    t.string "url"
    t.integer "timestamp"
  end

  create_table "shots", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "shot_id"
    t.string "remote_image"
    t.string "file_type"
    t.string "url"
    t.integer "timestamp"
    t.boolean "is_hd", default: true
    t.boolean "is_gif", default: false
    t.boolean "processed", default: false
    t.boolean "visible", default: true
    t.datetime "scheduled_for"
    t.string "remote_image_hd"
  end

  create_table "tweets", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "tweet_id"
    t.string "text"
    t.integer "timestamp"
  end

  create_table "videos", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "video_id"
    t.string "image"
    t.string "url"
    t.integer "timestamp"
    t.boolean "is_hd", default: true
  end

end
