require 'open-uri'
require 'digest/md5'
require 'fileutils'

class Downloader

  def initialize(options)
    @destination = options[:destination]
    verify_tmp_directory
  end

  def get(url)
    # puts; puts "[Download] Start `#{url}`..."
    downloaded_file = open(url)
    new_file = File.join(@destination, filename(url))
    IO.copy_stream(downloaded_file, new_file)
    # puts "[Download] Done `#{url}`"
    yield(new_file)
  rescue => e
    # puts "[Download] Failed `#{url}`"
    p e
  end

  private

  def verify_tmp_directory
    unless File.exists?(@destination)
      FileUtils.mkdir_p(@destination)
    end
  end

  def filename(url)
    Digest::MD5.hexdigest(url + Time.now.to_i.to_s)[0...16] + File.extname(url).downcase
  end

end
