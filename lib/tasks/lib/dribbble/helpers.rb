def shot_images(shot)
  [shot['images']['normal'], shot['images']['hidpi']]
end

def is_animated_shot(image_url)
  File.extname(image_url).downcase == '.gif'
end
