require 'fileutils'
require 'mini_magick'

class ImageProcessor

  def initialize(base_image, options)
    @base_image = base_image
    @name = options[:rename]
    @origin_dir = File.dirname(@base_image)
    @target_dir = options[:save_to]
    @files_list = {}
    verify_target_directory
  end

  def run
    return false unless process_image && move_files
    yield(@type)
  end

  private

  def verify_target_directory
    unless File.exists?(@target_dir)
      FileUtils.mkdir_p(@target_dir)
    end
  end

  def move_files
    # p @files_list
    @files_list.each_pair do |origin, target|
      FileUtils.mv File.join(@origin_dir, origin), File.join(@target_dir, target)
    end
    true
  rescue => e
    p e
    false
  end

  def process_image
    # puts 'Processing...'
    add_file_to_move @base_image
    if File.extname(@base_image) == '.gif'
      extract_frame
    else
      @type = File.extname(@base_image)[1..-1].to_sym
    end
  rescue => e
    p e
    false
  end

  def extract_frame
    frame_filename = @base_image.gsub(/\.gif$/, '_f.jpg')
    image = MiniMagick::Image.open(@base_image)
    if image.frames.first.write(frame_filename)
      add_file_to_move frame_filename, '_f'
      @type = :jpg
    end
  rescue => e
    p e
    false
  end

  def renamed_filename(file, suffix=nil)
    name = @name.to_s.dup
    name << suffix if suffix
    name << File.extname(file)
  end

  def add_file_to_move(file, suffix=nil)
    @files_list[File.basename(file)] = renamed_filename(file, suffix)
  end

end
