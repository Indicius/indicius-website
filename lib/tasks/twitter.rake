require 'rake'

# TODO: Email notification cuando hay un error

namespace :data do
  namespace :twitter do

    desc "Update Tweets."
    task :update_tweets => [:environment, :delete_deleted_tweets, :add_new_tweets, :update_grid_items, :delete_older_tweets] do
    end

    task :add_new_tweets => [:environment] do
      latest_tweet = Tweet.latest
      params = latest_tweet.respond_to?(:tweet_id) ? { since_id: latest_tweet.tweet_id } : {}
      tweets = Api::Twitter.tweets(params)

      if tweets.nil?
        puts "Failed to retrieve new tweets."
        next
      end

      puts "Found #{tweets.size} new tweets."

      next if tweets.empty?

      tweets.reverse.each do |tweet|
        Tweet.create!(
          tweet_id: tweet.id,
          text: tweet.text,
          timestamp: Time.parse(tweet.created_at.to_s).utc.to_i
        )
      end

      GridItem.clear_cache
    end

    task :update_grid_items => [:environment] do
      Tweet.update_positions
    end

    task :delete_older_tweets => [:environment] do
      Tweet.where(id: Tweet.order('timestamp DESC').offset(Api::Twitter.amount_to_keep).pluck(:id)).delete_all
    end

    task :delete_deleted_tweets => [:environment] do
      db_tweets = Tweet.pluck(:tweet_id)
      tweets = Api::Twitter.tweets.map(&:id)

      tweets_to_delete = db_tweets - tweets

      puts "Found #{tweets_to_delete.size} tweets in this DB to delete (that they're no longer on Indicius Twitter timeline)."
      Tweet.where(tweet_id: tweets_to_delete).delete_all
    end

  end
end
