require 'rake'

# TODO: Email notification cuando hay un error

namespace :data do
  namespace :vimeo do

    desc "Update Vimeo videos."
    task :update_videos => [:environment] do
      page_number = 1
      completed = false
      videos_to_save = []

      until completed do
        sleep 2 if page_number > 1

        reponse = Api::Vimeo.videos(page: page_number)
        break unless reponse

        reponse['data'].each do |video|
          videos_to_save << video_hash(video)
        end

        if (page_number * reponse['per_page']) >= reponse['total']
          completed = true
        end

        page_number += 1
      end

      if reponse.nil? && videos_to_save.size == 0
        puts "Failed to retrieve data."
        next
      end

      puts "Found #{videos_to_save.size} videos."
      next if videos_to_save.empty?

      ActiveRecord::Base.connection.execute("TRUNCATE TABLE #{Video.table_name}")
      videos_to_save.reverse.each do |video|
        Video.create! video
      end

      GridItem.clear_cache
    end

    def video_id video
      video['uri'].split('/').last.to_i
    end

    def video_image_and_quality pictures
      return [(pictures['sizes'].last)['link'], false] if pictures['sizes'].size < 4
      [pictures['sizes'][3]['link'], true]
    end

    def video_hash video
      image, is_hd = video_image_and_quality(video['pictures'])
      {
        video_id: video_id(video),
        image: image,
        url: video['link'],
        timestamp: Time.parse(video['created_time']).utc.to_i,
        is_hd: is_hd
      }
    end

  end
end
