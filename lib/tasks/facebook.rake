require 'rake'

# TODO: Email notification cuando hay un error

namespace :data do
  namespace :facebook do

    desc "Update Facebook facts."
    task :update_facts => [:environment] do
      page = Api::Facebook.page
      (puts "Failed to retrieve data."; next) if page.nil?
      (puts "Request failed."; next) if page['error']
      (puts "Request missing information."; next) unless page['fan_count'] && page['link']

      fact = FacebookFact.find_or_create_by({})
      fact.update!(data: {
        likes: page['fan_count'],
        url: page['link']
      })

      puts "Updated to #{page['fan_count']} likes."
      GridItem.clear_cache
    end

  end
end
