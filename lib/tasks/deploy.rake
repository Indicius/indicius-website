require 'rake'
require 'slack-notifier'

if Rails.env.development?
  require 'net/ssh'
end

namespace :deploy do

  desc 'Deploys a new version on PRODUCTION'
  task :production do
    ssh_deploy server: 'indicius_production_do', env: 'production'
  end

  desc 'Deploys a new version on STAGING'
  task :staging do
    ssh_deploy server: 'indicius_staging_do', env: 'staging'
  end

  def ssh_deploy(args)
    # puts `ssh deployer@indicius_production_do './run_deploy.sh production'`
    begin
      puts 'Connecting...'
      Net::SSH.start(args[:server], 'deployer') do |session|
        puts "=== Connected to #{session.host} ==="

        channel = session.open_channel do |ch|
          ch.exec("./run_deploy.sh #{args[:env]}") do |channel, success|
            raise "Could not execute command" unless success

            ch.on_data do |c, data|
              $stdout.print data
            end

            ch.on_extended_data do |c, type, data|
              $stderr.print data
            end

            ch.on_close do
              puts "=== Closing session ==="
            end
          end
        end

        channel.wait
      end
    rescue => e
      puts e
    end
  end

  namespace :remote do

    @revision_file = Rails.root.join('shared', 'last_revision')
    @lock_file = Rails.root.join('shared', 'deploy_lock')

    def read_deployed_commit
      File.open(@revision_file).read.chomp
    rescue
      ''
    end

    def write_deployed_commit(hash)
      File.write @revision_file, hash
    end

    def is_lock_in_place?
      File.exist? @lock_file
    end

    def add_lock
      File.write @lock_file, '.'
    end

    def remove_lock
      if is_lock_in_place?
        File.delete @lock_file
      end
    end

    def environment_branch
      return 'release' if Rails.env.production?
      return 'stage' if Rails.env.staging?
      'master'
    end

    desc 'Deploys new version'
    task update: [
      :environment,
      :check_for_lock,
      :check_if_required,
      :lock,
      :update_source,
      :stop_server,
      :bundle,
      :run_migrations,
      :compile_assets,
      :clear_cache,
      :start_server,
      :register_deploy,
      # :send_notification,
      :unlock
    ]

    desc 'Forces the deploy lock'
    task force_lock: [
      :environment,
      :lock
    ]

    desc 'Forces the deploy lock removal'
    task force_unlock: [
      :environment,
      :unlock
    ]

    task :check_for_lock do
      if is_lock_in_place?
        puts 'Aborting. Another deploy is active.'
        exit
      end
    end

    task :check_if_required do
      puts '> Checking for changes...'
      puts `git fetch origin`
      @commit = `git rev-parse --short origin/#{environment_branch}`.chomp
      @deployed_commit = read_deployed_commit
      if @commit == @deployed_commit
        puts 'Aborting. Already up to date.'
        exit
      end
    end

    task :lock do
      puts '> Adding lock...'
      add_lock
    end

    task :update_source do
      puts '> Updating...'
      puts `git pull`
      @commit = `git rev-parse --short #{environment_branch}`.chomp
    end

    task :bundle do
      puts '> Updating dependencies...'
      puts `RAILS_ENV=#{Rails.env} bundle`
    end

    task :run_migrations do
      puts '> Running migrations...'
      puts `RAILS_ENV=#{Rails.env} rake db:migrate`
    end

    task :compile_assets do
      puts '> Precompiling assets...'
      puts `RAILS_ENV=#{Rails.env} rake assets:precompile`
    end

    task :clear_cache do
      puts '> Clearing cache...'
      puts `RAILS_ENV=#{Rails.env} rake tmp:cache:clear`
    end

    task :stop_server do
      puts '> Killing unicorn...'
      puts `sudo service unicorn stop; sudo service unicorn stop`
    end

    task :start_server do
      puts '> Starting unicorn...'
      puts `sudo service unicorn start`
    end

    task :register_deploy do
      puts '> Registering deploy...'
      puts `RAILS_ENV=#{Rails.env} newrelic deployments -r #{@commit}`
      write_deployed_commit(@commit)
    end

    task :send_notification do
      puts '> Sending notification...'
      slack = Slack::Notifier.new 'https://hooks.slack.com/services/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', channel: '#indi-revamp'
      message = "Deployed revision <https://bitbucket.org/indicius/indicius-2015/commits/#{@commit}|#{@commit}> to *#{Rails.env}*"
      slack.ping message
    end

    task :unlock do
      puts '> Removing lock...'
      remove_lock
    end

  end
end
