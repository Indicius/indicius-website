require 'rake'
require_relative 'lib/dribbble/downloader'
require_relative 'lib/dribbble/image_processor'
require_relative 'lib/dribbble/helpers'

# TODO: Email notification cuando hay un error

namespace :data do
  namespace :dribbble do

    desc "Update Dribbble Facts."
    task :update_facts => [:environment] do
      team = Api::Dribbble.team
      (puts "Failed to retrieve data."; next) if team.nil?
      fact = DribbbleFact.find_or_create_by({})
      fact.update!(data: {
        followers:  team['followers_count'],
        shots:      team['shots_count'],
        members:    team['members_count'],
        url:        team['html_url']
      })

      puts "Updated to #{team['followers_count']} followers"\
        ", #{team['shots_count']} shots"\
        " and #{team['members_count']} members."

      GridItem.clear_cache
    end

    desc "Import latest shots."
    task :update_shots => [:environment] do
      latest_shot = Shot.latest
      latest_id = latest_shot.shot_id if latest_shot.respond_to?(:shot_id)
      shots_to_save = []
      page_number = 1
      completed = false
      shots_limit = nil

      # TODO: Caso de shots sin imágenes en high.

      until completed do
        shots = Api::Dribbble.shots(page: page_number)
        break if shots.nil? || shots.empty?

        shots.each do |shot|
          if shot['id'] == latest_id
            completed = true
            break
          end
          image, image_hd = shot_images(shot)

          scheduled_for = Time.parse(shot['scheduled_for']).utc rescue nil

          shots_to_save << {
            shot_id: shot['id'],
            remote_image: image,
            remote_image_hd: image_hd,
            url: shot['html_url'],
            timestamp: Time.parse(shot['published_at']).utc.to_i,
            scheduled_for: scheduled_for,
            is_hd: image_hd.present?,
            is_gif: is_animated_shot(image)
          }
          if !shots_limit.nil? && shots_to_save.size >= shots_limit
            puts "Limit of #{shots_limit} reached."
            completed = true
            break
          end
        end

        page_number += 1
      end

      if shots.nil?
        puts "Failed to retrieve data."
        next
      end

      puts "Found #{shots_to_save.size} new shots."
      next if shots_to_save.empty?

      shots_to_save.reverse.each do |shot|
        Shot.create! shot
      end

      GridItem.clear_cache
    end

    desc "update all remote images urls"
    task :update_shots_urls => [:environment] do
      cont = 0
      page_number = 1
      completed = false
      until completed do
        dribbble_shots = Api::Dribbble.shots(page: page_number)
        
        completed = dribbble_shots.nil? || dribbble_shots.empty?
        
        dribbble_shots.each do |dribbble_shot|

          image, image_hd = shot_images(dribbble_shot)
          shot = Shot.find_by(shot_id: dribbble_shot['id'])
          next if shot.nil?
          shot.update_attributes({
            remote_image: image,
            remote_image_hd: image_hd,
            is_hd: image_hd.present?
          })
          cont += 1
        end

        page_number += 1

      end

      puts "#{cont} shots updated"

    end

    desc "Process images from existing shots."
    task :process_images => [:environment] do

      # Download image to tmp location
      # Process Image
      # - Optimize image
      # - If Gif
      #   - save frame
      # Move final version(s) to public folder
      # Update shot: `processed` flag

      shots = Shot.where(processed: false)
      process_images(shots)

    end

    desc "Process all images from existing shots."
    task :reprocess_all_images => [:environment] do

      shots = Shot.all
      process_images(shots)
      
    end

    def process_images(shots)
      if shots.any?
        puts "Processing #{shots.size} images."
      else
        puts 'Nothing to process.'
        return
      end

      tmp_dir   = Rails.root.join('tmp', 'downloads')
      final_dir = Shot.media_path(Rails.root.join('public'))
      downloader = Downloader.new(destination: tmp_dir)

      shots.each do |shot|
        
        puts "> #{shot.shot_id}"
        downloader.get(shot.remote_image) do |image|
          process_image(image, final_dir, shot, shot.shot_id)
        end

        unless shot.remote_image_hd.nil?
          puts "> #{shot.shot_id} hd"
          downloader.get(shot.remote_image_hd) do |image|
            process_image(image, final_dir, shot, "#{shot.shot_id}@2x")
          end
        end

      end

      puts 'Completed.'
    end

    def process_image(image, final_dir, shot, file_name)
        puts '  downloaded'
        processor = ImageProcessor.new(
          image,
          rename: file_name,
          save_to: final_dir
        )
        processor.run do |type|
          puts '  processed'
          shot.update!(
            processed: true,
            file_type: type
          )
        end
    end

  end
end
