class Numeric
  def not_negative
    return 0 if self < 0
    self
  end
end
